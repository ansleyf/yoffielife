function cBoardAjax(id,request,doing,onboard) {
	$("[data-cboard=" + id + "] i").removeClass(doing).addClass("fa fa-spinner fa-spin");
	$.ajax({
		url: request + "&onboard="+onboard,
		dataType: "json"
	}).done(function(data) {
		switch(doing) {
			case "add":
				$("[data-cboard=" + id + "]").html(data.html);
			break;
			case "complete":
				if(onboard == "onboard") {
					$("li.id-"+id).prependTo(".vic").fadeIn();
					$("[data-cboard=" + id + "]").html(data.html);
					$(".goal h2 .count").html(data.mgoalc);
					if(isNaN(parseInt(data.alltime)) == false) {
						$(".total h2 .count").html(data.alltime);
					}
				} else {
					$("[data-cboard=" + id + "]").html(data.html);
				}
			break;
			case "remove":
				if(onboard == "onboard") {
					$("li.id-"+id).fadeOut().delay(500).remove();
				}
			break;
		}
	});
}

function cBoardSetGoal(goal,loc) {
	$(loc+" form").fadeOut();
	$(loc+" fa fa-spinner fa-spin").show();
	$.ajax({
		url: '?cboard=setgoal&goal='+goal,
		dataType: 'text',
	})
	.done(function() {
		$(loc+" fa fa-spinner fa-spin").hide();
		$(loc+" .cLoader.complete").show().delay(1000).fadeOut();
		$(loc+" #update").show();
	})
	.fail(function() {
		$(loc+" .cLoader.remove").show().delay(1000).fadeOut();
		$(loc+" #update").show();
	});
}

$(function() {
	$("html").on('click', '.cBoard:not(.link)', function(event) {
		event.preventDefault();
		var id = $(this).attr("id");
		var href = $(this).attr("href");
		var doing = $("[data-cboard=" + id + "] a").attr('class').split(' ')[1];
		var onboard = $("[data-cboard=" + id + "] a").attr('class').split(' ')[2];
		cBoardAjax(id, href, doing, onboard);
	});
	$(".goalupdate").on('click', '#update', function(event) {
		event.preventDefault();
		$(".goalupdate form").fadeIn();
		$(".goalupdate #update").hide();
	});
	$(".goalupdate").on('submit', '#updategoalform', function(event) {
		event.preventDefault();
		var goal = $("#updategoalform input[name=goal]").val();
		cBoardSetGoal(goal, ".goalupdate");
	});
	$(".setgoalbanner").on('submit', '#updategoalformb', function(event) {
		event.preventDefault();
		var goal = $("#updategoalformb input[name=goal]").val();
		cBoardSetGoal(goal, ".setgoalbanner");
		window.location = "http://"+window.location.host+"/challenge-board";
	});
});