<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<?php wp_nav_menu( array( 'theme_location' => 'disclaimer-menu' ) ); ?>
				&copy; 2014 Yoffie Life &bull; <a href="http://ansleyfones.com" target="_blank">Web Development by <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/af-logo-very-small-grey.png" style="vertical-align: bottom;">Ansley Fones</a>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

<!-- hides loading gif -->
	<?php wp_footer(); ?>
</body>
</html>
