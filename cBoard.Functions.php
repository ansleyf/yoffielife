<?php
	function cboard_ajax() {
		$ajax = new challengeboard;
		if(isset($_GET["cboard"])) {
			if($_GET["cboard"] == "add") {
				$ajax->add_challenge($_GET["postid"]);
			} elseif($_GET["cboard"] == "complete") {
				$ajax->complete_challenge($_GET["postid"], $_GET["onboard"]);
			} elseif($_GET["cboard"] == "remove") {
				$ajax->remove_challenge($_GET["postid"]);
			} elseif($_GET["cboard"] == "clear") {
				$ajax->clear_challengeboard();
			} elseif($_GET["cboard"] == "setgoal") {
				$ajax->set_monthlygoal($_GET["goal"]);
			}
			exit();
		}
	}
	add_action("template_redirect", "cboard_ajax");

	function cboard_link($action = null, $onboard = false, $postid = null) {
		$cboard = new challengeboard;
		echo '<div class="favorites-link" data-cBoard="'.$cboard->null_postid($postid).'">';
		echo $cboard->build_link($action, $onboard, $postid);
		echo '</div>';
	}

	function get_post_points($postid = null) {
		$cboard = new challengeboard;
		return $cboard->get_post_points($postid);
	}
	function the_badges() {
		$cboard = new challengeboard;
		$cboard->the_badges();
	}
?>