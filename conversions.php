<?php
/*
Template Name: Conversion Script
*/
	get_header();
	function get_attachment_id_by_url( $url ) {
		$parse_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
		$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
		$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
		if ( ! isset( $parse_url[1] ) || empty( $parse_url[1] ) || ( $this_host != $file_host ) ) {
			return;
		}
		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parse_url[1] ) );
		return $attachment[0];
	}
	$args = $arrayName = array(
		'posts_per_page'	=> -1,
		'post_type'			=> 'post',
		);
	query_posts($args);
	// $pod = pods('post');
	while ( have_posts() ) : the_post();
		$data['name'] = $post->post_title;
		// $data['slug'] = $post->post_name;
		// $data['post_data'] = $post->post_date;
		// $data['post_status'] = $post->post_status;
		// $data['author'] = $post->post_author;
		// $posttags = get_the_terms($post->ID, 'post_tag');
		// if ($posttags) {
		// 	foreach($posttags as $tag) {
		// 		 $data['post_tag'][] = $tag->name; 
		// 	}
		// }
		// $postcats = get_the_terms($post->ID, 'category');
		// if($postcats) {
		// 	foreach($postcats as $cat) {
		// 		$data['category'][] = $cat->name;
		// 	}
		// }
		// $data['content'] = $post->post_content;
		// $data['featured'] = get_post_thumbnail_id();
		$data['the_facts'] = get_post_meta($post->ID, 'wpcf-the-facts', true);
		$data['the_symptoms'] = get_post_meta($post->ID, 'wpcf-the-symptoms', true);
		$data['what_is_happening'] = get_post_meta($post->ID, 'wpcf-what-is-happening', true);
		$data['why_is_this_happening'] = get_post_meta($post->ID, 'wpcf-why-is-this-happening', true);
		$data['lifestyle_adjustments'] = get_post_meta($post->ID, 'wpcf-lifestyle-adjustments', true);
		$data['prevent_it'] = get_post_meta($post->ID, 'wpcf-prevent-it', true);
		$data['fix_it'] = get_post_meta($post->ID, 'wpcf-fix-it', true);
		$data['exercises'] = get_post_meta($post->ID, 'wpcf-exercises', true);
		$data['variations'] = get_post_meta($post->ID, 'wpcf-variations', true);
		$data['beginners_tips'] = get_post_meta($post->ID, 'wpcf-beginners_tips', true);
		$data['health_benefits'] = get_post_meta($post->ID, 'wpcf-health_benefits', true);
		$data['yoffie_wisdom'] = get_post_meta($post->ID, 'wpcf-yoffie_wisdom', true);
		$data['additional_author_info'] = get_post_meta($post->ID, 'wpcf-additional-author-info', true);

		if(!empty($data['name'])){
			if(function_exists('pods')) {
				// $result = $pod->save($data, null, $post->ID);
				// if(!empty($data['featured'])) add_post_meta($result, '_thumbnail_id', $data['featured']);
			}
		} else {
			$data['name'] = "Skipped";
		}
		echo "<pre>";
			print_r($data);
		echo "</pre>";
		unset($data);
	endwhile;
	get_footer();