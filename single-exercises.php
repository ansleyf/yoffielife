<?php
/*
Template Name: Exercise
*/
?>

<?php get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
		
		

		<h1 class="entry-title"><?php the_title(); ?></h1>
		

		<div class="entry-meta">
			<?php cboard_link() ?> 
			<?php twentythirteen_entry_meta(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	
	<?php echo do_shortcode('[social_buttons]'); ?>

	<div class="entry-content">
		
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

		<div class="exercise-wrapper">
		<div class="exercise-photos">

		<?php $firstphoto = get_post_meta($post->ID, 'image_1', true); ?>
		<?php $secondphoto = get_post_meta($post->ID, 'image_2', true); ?>
		<?php $thirdphoto = get_post_meta($post->ID, 'image_3', true); ?>
		<?php $fourthphoto = get_post_meta($post->ID, 'image_4', true); ?>
		<?php $comingimage = get_post_meta($post->ID, 'coming_from_exercise_image', TRUE); ?>
		<?php $comingurl = get_post_meta($post->ID, 'coming_from_exercise_url', TRUE); ?>
		<?php $goingimage = get_post_meta($post->ID, 'going_into_exercise_image', TRUE); ?>
		<?php $goingurl = get_post_meta($post->ID, 'going_into_exercise_url', TRUE); ?>
		<?php $comingtitle = get_post_meta($post->ID, 'coming_from_exercise_title', TRUE); ?>
		<?php $goingtitle = get_post_meta($post->ID, 'going_into_exercise_title', TRUE); ?>

		<?php if (!$fourthphoto) {
				if (!$thirdphoto) {
					if (!$secondphoto) {
					echo '<div class="one-photo"><div class="exercise-grid"><img src="'; echo $firstphoto['guid']; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div></div>';
					} else {
					echo '<div class="two-photos"><div class="exercise-grid"><img src="'; echo $firstphoto['guid']; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $secondphoto['guid']; echo'"><div class="exercise-caption">'; echo $secondcaption; echo '</div></div></div>';
					}
				} else {
				echo '<div class="three-photos"><div class="exercise-grid"><img src="'; echo $firstphoto['guid']; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $secondphoto['guid']; echo'"><div class="exercise-caption">'; echo $secondcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $thirdphoto['guid']; echo'"><div class="exercise-caption">'; echo $thirdcaption; echo '</div></div></div>';
				}
		      	} else {
			echo '<div class="four-photos"><div class="exercise-grid"><img src="'; echo $firstphoto['guid']; echo'"></div><div class="exercise-grid"><img src="'; echo $secondphoto['guid']; echo'"></div><div class="exercise-grid"><img src="'; echo $thirdphoto['guid']; echo'"></div><div class="exercise-grid"><img src="'; echo $fourthphoto['guid']; echo'"></div>';
		} ?>
		
		</div><!-- end exercise-photos -->

		<div class="exercise-instructions">
		<?php $instructions = get_post_meta($post->ID, 'instructions', TRUE); ?>
		<?php echo $instructions; ?>
		</div>

		</div>

		<div class="related-exercises">
			<?php if (!$comingurl) { echo ""; } else { ?>
			<div class="coming-from">
				<h4>Come into this exercise from...</h4>
				<div class="related-image"><a href="<?php echo $comingurl; ?>"><img src="<?php echo $comingimage['guid']; ?>"><br/><?php echo $comingtitle; ?></a></div>
			</div>
			<?php } ?>
			<?php if (!$goingurl) { echo ""; } else { ?>
			<div class="going-to">
				<h4>Proceed from here into...</h4>
				<div class="related-image"><a href="<?php echo $goingurl; ?>"><img src="<?php echo $goingimage['guid']; ?>"><br/><?php echo $goingtitle; ?></a></div>
			</div>
			<?php } ?>
		</div>

	<div class="extra-drop-downs">
		<?php $anatomical = get_post_meta($post->ID, 'anatomical_focus', TRUE); ?>
		<?php $benefit = get_post_meta($post->ID, 'benefits', TRUE); ?>
		<?php $therapy = get_post_meta($post->ID, 'therapeutic_application', TRUE); ?>
		<?php $btips = get_post_meta($post->ID, 'beginners_tips', TRUE); ?>
		<?php $modifications = get_post_meta($post->ID, 'modifications', TRUE); ?>
		<?php $variations = get_post_meta($post->ID, 'variations', TRUE); ?>
		<?php $wisdom = get_post_meta($post->ID, 'yoffie_wisdom', TRUE); ?>
		<?php $related = get_post_meta($post->ID, 'related_exercises', TRUE); ?>

		<?php if (!$anatomical){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Anatomical Focus" id="anatomical" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Anatomical Focus"]' . $anatomical . '[/expand]'); 
			} ?>
		<?php if (!$benefit){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Benefit" id="benefits" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Benefit"]' . $benefit . '[/expand]'); 
			} ?>
		<?php if (!$therapy){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Therapy" id="therapy" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Therapy"]' . $therapy . '[/expand]'); 
			} ?>
		<?php if (!$btips){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Beginner\'s Tips" id="tips" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Beginner\'s Tips"]' . $btips . '[/expand]'); 
			} ?>
		<?php if (!$modifications){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Modifications" id="modifications" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Modifications"]' . $modifications . '[/expand]'); 
			} ?>
		<?php if (!$variations){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Variations" id="variations" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Variations"]' . $variations . '[/expand]'); 
			} ?>
	
		<?php if (!$wisdom){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Yoffie Wisdom" id="wisdom" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Yoffie Wisdom"]' . $wisdom . '[/expand]'); 
			} ?>
		<?php if (!$related){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Related Exercises" id="related" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Related Exercises"]' . $related . '[/expand]'); 
			} ?>

	</div><!-- end extra-drop-downs -->
		
	</div><!-- .entry-content -->
	<div class="second-social"><?php echo do_shortcode('[social_buttons]'); ?></div>
	<div class="disclaimer">This information is not intended to replace the advice of a doctor. Yoffie Life disclaims any liability for the decisions you make based on this information.</div>
	<footer class="entry-meta">
		<?php twentythirteen_cats_tags(); ?>
		<?php comments_template(); ?> 
		
	</footer><!-- .entry-meta -->

	
</article><!-- #post -->
				<?php if (function_exists('nrelate_related')) nrelate_related(); ?>
				<?php twentythirteen_post_nav(); ?>
				<?php comments_template(); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>