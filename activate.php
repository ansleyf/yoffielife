<?php get_header( 'buddypress' ); ?>

	<div id="content">
		<div class="padder">

		<?php do_action( 'bp_before_activation_page' ); ?>

		<div class="page" id="activate-page">

			<h3><?php if ( bp_account_was_activated() ) :
				_e( 'Account Activated', 'buddypress' );
			else :
				_e( 'Activate your Account', 'buddypress' );
			endif; ?></h3>

			<?php do_action( 'template_notices' ); ?>

			<?php do_action( 'bp_before_activate_content' ); ?>

			<?php if ( bp_account_was_activated() ) : ?>

				<?php if ( isset( $_GET['e'] ) ) : ?>
					<p>Your account has been activated. You may now log in to the site using your chosen username. Please check your email inbox for your password and login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="http://yoffielife.com/wp-login.php?action=lostpassword">reset your password</a>.</p>
					<hr/>
					<p><a title="This Week’s Challenges" href="http://yoffielife.com/this-weeks-challenges/">Set Your Goals.</a><br/>
					<a title="Challenge Board" href="http://yoffielife.com/challenge-board/">Build a Challenge Board.</a><br/>
					<a title="Community" href="http://yoffielife.com/community/">Share Your Success With The Yoffie Life Community.</a><br/>
					<a title="Meet the Experts" href="http://yoffielife.com/meet-the-experts/">Interact With Yoffie Life Experts.</a><br/>
					<a title="About" href="http://yoffielife.com/about/">Experience How Small Changes = Big Victories.</a>
					</p>
				<?php endif; ?>

			<?php else : ?>

				<p><?php _e( 'Please provide a valid activation key.', 'buddypress' ); ?></p>

				<form action="" method="get" class="standard-form" id="activation-form">

					<label for="key"><?php _e( 'Activation Key:', 'buddypress' ); ?></label>
					<input type="text" name="key" id="key" value="" />

					<p class="submit">
						<input type="submit" name="submit" value="<?php esc_attr_e( 'Activate', 'buddypress' ); ?>" />
					</p>

				</form>

			<?php endif; ?>

			<?php do_action( 'bp_after_activate_content' ); ?>

		</div><!-- .page -->

		<?php do_action( 'bp_after_activation_page' ); ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php get_sidebar( 'buddypress' ); ?>

<?php get_footer( 'buddypress' ); ?>
