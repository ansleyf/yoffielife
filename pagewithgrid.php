<?php
/*
Template Name: Page with Challenges
*/

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

			<div class="entry-content">
			<?php the_content(); ?>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
			<hr/>
			<h2>Challenge Yourself Today!</h2>
			<?php query_posts('showposts=12&cat=-1172'); ?>
			<?php get_template_part( 'triple-grid' ); ?> 
			<?php wp_reset_query(); ?>

		</div><!-- .entry-content -->

		</article><!-- #post -->
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>