<?php
/**
Template Name: Forums
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); global $currpage; $currpage = get_the_ID(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						
						
						
					</div><!-- .entry-content -->

					
				</article><!-- #post -->

			<?php endwhile; ?>
			
			<ul class="triple-grid">
				<li ><a href="/communities/#!/satisfy" class="satisfy"><div class="category-label">Satisfy</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/satisfy-forums.png"></a></li>
				<li ><a href="/communities/#!/simmer" class="simmer"><div class="category-label">Simmer</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/simmer-forums.png"></a></li>
				<li ><a href="/communities/#!/simplify" class="simplify"><div class="category-label">Simplify</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/simplify-forums.png"></a></li>
				<li ><a href="/communities/#!/soul" class="soul"><div class="category-label">Soul</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/soul-forums.png"></a></li>
				<li ><a href="/communities/#!/spark" class="spark"><div class="category-label">Spark</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/spark-forums.png"></a></li>
				<li ><a href="/communities/#!/sweat" class="sweat"><div class="category-label">Sweat</div><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/forums/sweat-forums.png"></a></li>
			</ul>


		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>