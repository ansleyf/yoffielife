<?php
/*
Template Name: Workout
*/
?>

<?php get_header(); ?>
<?php get_sidebar(); ?>
    <div id="primary" class="content-area">
        <div id="content" class="site-content workout-content" role="main">

            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <?php
                $info = get_post_meta(get_the_ID(), '_workout_info', true);
                $exercises = array();

                if ($info) foreach ($info['exercises'] as $ex) {
                    array_push($exercises, get_post($ex));
                }
                ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">

                        <div class="workout-title">
                            <h1 class="entry-title"><?php the_title(); ?></h1>

                            <div class="buttons">
                                <div class="cboard-wrapper" title="Add to Challenge Board"><?php cboard_link() ?></div>
                                <a href="#" rel="nofollow" class="share" title="Share"><i class="fa fa-share"></i></a>
                            </div>
                        </div>

                        <?php echo do_shortcode('[social_buttons]'); ?>
                    </header><!-- .entry-header -->

                    <div class="entry-content">

                        <div class="workout-entry clearfix">
                            <?php $first = true; foreach ($exercises as $ex) : ?>
                                <?php
                                $photos = array();
                                for ($i = 1; $i <= 4; $i++) {
                                    $meta = get_post_meta($ex->ID, 'image_' . $i, true);
                                    if ($meta) array_push($photos, $meta);
                                }
                                ?>

                                <div class="main-image column exercise-info entry-<?php echo $ex->ID ?><?php if ($first) echo ' active' ?>">
                                    <?php $firstPhoto = true; foreach ($photos as $index => $photo) : ?>
                                        <img src="<?php echo $photo['guid'] ?>" alt="<?php echo $ex->post_title ?>" class="instruction instruction-<?php echo $photo['ID'] ?><?php if ($firstPhoto) echo ' active' ?>"/>
                                    <?php $firstPhoto = false; endforeach; ?>
                                </div>
                            <?php $first = false; endforeach; ?>

                            <?php $first = true; foreach ($exercises as $ex) : ?>
                                <div class="main-info column exercise-info entry-<?php echo $ex->ID ?><?php if ($first) echo ' active' ?>">
                                    <div class="exercise-title"><?php echo $ex->post_title ?></div>

                                    <?php
                                    $photos = array();
                                    for ($i = 1; $i <= 4; $i++) {
                                        $meta = get_post_meta($ex->ID, 'image_' . $i, true);
                                        if ($meta) array_push($photos, $meta);
                                    }
                                    ?>

                                    <div class="instructions">
                                        <?php $firstPhoto = true; foreach ($photos as $index => $photo) : ?>
                                            <div class="instruction instruction-<?php echo $photo['ID'] ?><?php if ($firstPhoto) echo ' active' ?>">
                                                <?php echo $photo['post_title'] ?>
                                            </div>
                                        <?php $firstPhoto = false; endforeach; ?>
                                    </div>

                                    <div class="steps">
                                        <?php $firstPhoto = true; foreach ($photos as $index => $photo) : ?>
                                            <div class="step<?php if ($firstPhoto) echo ' active' ?>" data-target=".instruction-<?php echo $photo['ID'] ?>"><?php echo $index + 1?></div>
                                        <?php $firstPhoto = false; endforeach; ?>
                                    </div>
                                </div>
                            <?php $first = false; endforeach; ?>

                            <div class="meta column">
                                <?php $cats = get_the_terms( get_the_ID(), 'workout-category' ); ?>
                                <ul class="tags">
                                    <?php foreach ($cats as $cat) : ?>
                                    <li><a href="<?php echo get_term_link($cat, 'workout-category'); ?>"><?php echo $cat->name ?></a></li>
                                    <?php endforeach; ?>
                                </ul>

                                <?php $first = true; foreach ($exercises as $k => $ex) : ?>
                                    <ul class="info exercise-info entry-<?php echo $ex->ID ?><?php if ($first) echo ' active' ?>">
                                        <?php if ($info['suggested_reps'][$k]) : ?>
                                            <li class="clearfix">
                                                <div class="name">Suggested reps</div>
                                                <div class="value"><?php echo $info['suggested_reps'][$k] ?></div>
                                            </li>
                                        <?php endif; ?>
                                        <?php if ($info['seconds_each'][$k]) : ?>
                                            <li class="clearfix">
                                                <div class="name">Seconds each</div>
                                                <div class="value"><?php echo $info['seconds_each'][$k] ?></div>
                                            </li>
                                        <?php endif; ?>
                                        <?php if ($info['suggested_sets'][$k]) : ?>
                                            <li class="clearfix">
                                                <div class="name">Suggested sets</div>
                                                <div class="value"><?php echo $info['suggested_sets'][$k] ?></div>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                <?php $first = false; endforeach; ?>
                            </div>
                        </div>

                        <div class="workout-slider clearfix">
                            <ul class="slides">
                                <?php $first = true; foreach ($exercises as $ex) : ?>
                                    <li class="exercise<?php if ($first) echo ' active' ?>" data-target=".entry-<?php echo $ex->ID ?>">
                                        <div class="cboard-wrapper" title="Add to Challenge Board"><?php cboard_link(null, false, $ex->ID) ?></div>

                                        <div class="image">
                                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ex->ID ), 'full' ); ?>
                                            <img src="<?php echo $image[0] ?>" alt="<?php echo $ex->post_title ?>"/>
                                        </div>

                                        <div class="title">
                                            <?php echo $ex->post_title ?>
                                        </div>
                                    </li>
                                <?php $first = false; endforeach; ?>
                            </ul>
                        </div>

                        <div class="workout-author clearfix">
                            <div class="image">
                                <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/workout3.png" alt=""/></a>
                            </div>

                            <div class="content">
                                <?php $author = get_the_author_meta('ID');?>
                                <div class="social">
                                    <?php $facebook = get_user_meta($author, 'facebook', true); if ($facebook) : ?>
                                        <a href="<?php echo $facebook ?>" class="top-social"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/social/facebook-small.png"></a>
                                    <?php endif; ?>
                                    <?php $twitter = get_user_meta($author, 'twitter', true); if ($twitter) : ?>
                                        <a href="https://twitter.com/<?php echo $twitter ?>" class="top-social"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/social/twitter-small.png"></a>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="name">
<!--                                    <a href="--><?php //echo get_author_posts_url($author) ?><!--">--><?php //the_author_meta( 'display_name' ); ?><!--</a>-->
                                </div>

                                <?php $desc = get_user_meta($author, 'description', true); if ($desc) : ?>
                                    <div class="description">
                                        <?php echo $desc ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div><!-- .entry-content -->

                </article>

            <?php endwhile; ?>

        </div><!-- #content -->
    </div><!-- #primary -->

<link href="<?php echo get_stylesheet_directory_uri() ?>/css/flexslider.css" rel="stylesheet"/>
<script src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery.flexslider-min.js"></script>
<script>
    jQuery(function ($) {
        $('.workout-content .share').click(function (e) {
            e.preventDefault();

            $('#social-share').toggle();
        })

        $('.workout-entry').on('click', '.step', function (e) {
            e.preventDefault();

            $('.workout-entry .step').removeClass('active');
            $(this).addClass('active');

            $('.workout-entry .instruction').removeClass('active');
            $($(this).data('target')).addClass('active');
        })

        $('.workout-slider').on('click', '.exercise', function (e) {
            e.preventDefault();

            $('.workout-slider .exercise').removeClass('active');
            $(this).addClass('active');

            $('.workout-entry .exercise-info').removeClass('active');
            $($(this).data('target')).addClass('active').find('.step').eq(0).click();
        })

        $('.workout-slider').flexslider({
            animation: "slide",
            useCSS: false,
            controlNav: false,
            slideshow: false,
            nextText: '',
            prevText: '',
            itemWidth: 120,
            itemMargin: 2
        });
    })
</script>

<?php get_footer(); ?>