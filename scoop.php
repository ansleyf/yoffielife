<?php
/**
Template Name: Scoop
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area category-page">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php query_posts('category_name=yoffie-scoop&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>"><header class="entry-header">
						<?php if ( ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<div class="title scoop">This Week's Scoop Challenge</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<?php endif; ?>
						<div class="entry-container">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
							<div class="view scoop">View Scoop Challenge</div>
						</div>
					</header></a><!-- .entry-header -->

				</article><!-- #post -->

	
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			
			
			<section id="yoffie-blog">
			<?php query_posts('category_name=yoffie-scoop&showposts=12&offset=1'); ?>
			<?php get_template_part( 'triple-grid' ); ?>
			<?php wp_reset_query(); ?>	
			</section>
	

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>