<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Search Results for: %s', 'twentythirteen' ), get_search_query() ); ?></h1>
			</header>

			<?php /* The loop */ ?>
			<ul class="triple-grid">
					<?php while ( have_posts() ) : the_post(); ?>
						<li id="<?php the_ID(); ?>">
						<div class="category-thumbnail">
						<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/placeholder.png">'; } ?></a>
						</div>
					
						<div class="triple-grid-overlay">
						<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
						<?php cboard_link() ?> 
						</div>
						</li>
					<?php endwhile; ?>
			</ul>
			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>