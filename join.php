<?php
/*
Template Name: Join
*/

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); global $currpage; $currpage = get_the_ID(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<strong>Join with Facebook or Twitter</strong>
						<?php do_action('oa_social_login'); ?>
						<br/>
						<?php echo do_shortcode('[gravityform id="1" name="Registration" ajax="true"]'); ?>
						<div style="margin-top: 15px; font-size: 13px; line-height: 16px;">At Yoffie Life LLC, we respect your privacy.  The use of information collected through our site shall be limited to the purpose of providing the service for which you engaged Yoffie Life.  See our <a href="/privacy-policy/">privacy policy</a> for more information.</div>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					
				</article><!-- #post -->

				
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>