<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <!-- SLIDER -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/slider/css/zozo.tabs.min.css" rel="stylesheet"/>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/slider/js/jquery.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/slider/js/jquery.easing.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/slider/js/zozo.tabs.min.js"></script>
    <!-- end SLIDER -->
    <!-- Pushy -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/pushy/css/pushy.css">
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/pushy/js/modernizr.custom.71422.js"></script>
    <!-- end Pushy -->
    <!-- A73DBA4B-25F6-4A62-ABF4-23959B7B7FFD -->
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/community.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/newsletter.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet"/>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/cookie.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/cBoard.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modal.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/pushy/js/pushy.min.js"></script>
    <meta name="p:domain_verify" content="f04133fe4ed6251cf191c7026642a6c7"/>

    <?php wp_head(); ?>
    <script>
        $('#tabbed-nav').hide(0);
    </script>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/mobile.css?v=2" rel="stylesheet"/>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <div id="fade"></div>
    <?php if (!is_user_logged_in()) get_template_part("loginpopup"); ?>

    <div>
        <header id="masthead" class="site-header" role="banner">
            <div id="inner-header">
                <div class="store-search">
                    <div id="top-social-wrap"><?php if (!is_user_logged_in()) { ?><a href="/join" class="join-button">Join!</a><?php } ?>
                        <a href="https://www.facebook.com/YoffieLife" class="top-social"><img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/images/social/facebook-small.png"></a><a
                                href="https://twitter.com/YoffieLife" class="top-social"><img
                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/social/twitter-small.png"></a><a
                                href="http://www.pinterest.com/yoffielife/" class="top-social"><img
                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/social/pinterest-small.png"></a><a
                                href="http://instagram.com/yoffielife" class="top-social"><img
                                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/social/instagram-small.png"></a>
                    </div>

                    <div><?php get_search_form(); ?></div>
                    <div id="store-search-menu"><?php if (is_user_logged_in()) { ?><a
                                href="<?php echo wp_logout_url('$index.php'); ?>">Logout</a><?php } else { ?><a
                                id="logintrigger" href="#">Login</a><?php } ?> | <a href="/store">Store</a></div>
                </div>
                <!-- store search -->
                <a class="home-link" href="<?php echo esc_url(home_url('/')); ?>"
                   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                    <img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png">
                </a>
            </div>
            <!-- inner-header -->
            <div id="navbar" class="navbar">
                <div class="sidr-links">
                    <a href="#sidr-left" class="sidr-link" id="sidr-left-link"><img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/mobile-menu.png">
                        Challenge Categories</a>
                    <a href="#sidr-right" class="sidr-link" id="sidr-right-link">Menu <img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/mobile-menu.png"></a>
                </div>
                <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                    <h3 class="menu-toggle"><?php _e('Menu', 'twentythirteen'); ?></h3>
                    <a class="screen-reader-text skip-link" href="#content"
                       title="<?php esc_attr_e('Skip to content', 'twentythirteen'); ?>"><?php _e('Skip to content', 'twentythirteen'); ?></a>
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu')); ?>
                </nav>
                <!-- #site-navigation -->
            </div>
            <!-- #navbar -->
        </header>
        <!-- #masthead -->


        <?php if (is_front_page()) {
            echo "<div class='category-header'><a href='/'><img src='/wp-content/themes/twentythirteen-child/images/homepage-banner.png'></a></div>";
        } elseif (is_page(1011)) {
            echo "<div class='category-header'><a href='/challenge-board'><img src='/wp-content/themes/twentythirteen-child/images/challenge-board.png'></a></div>";
        } elseif ((is_page(891))) {
            echo "<div class='category-header'><a href='/services'><img src='/wp-content/themes/twentythirteen-child/images/services-header.png'></a></div>";
        } elseif ((is_single && in_category('soul')) || is_page(888) || is_category('soul')) {
            echo "<div class='category-header'><a href='/soul'><img src='/wp-content/themes/twentythirteen-child/images/soul-header.png'></a></div>";
        } elseif ((is_single && in_category('spark')) || is_page(890) || is_category('spark')) {
            echo "<div class='category-header'><a href='/spark'><img src='/wp-content/themes/twentythirteen-child/images/spark-header.png'></a></div>";
        } elseif ((is_single && in_category('simplify')) || is_page(887) || is_category('simplify')) {
            echo "<div class='category-header'><a href='/simplify'><img src='/wp-content/themes/twentythirteen-child/images/simplify-header.png'></a></div>";
        } elseif ((is_single && in_category('simmer')) || is_page(886) || is_category('simmer')) {
            echo "<div class='category-header'><a href='/simmer'><img src='/wp-content/themes/twentythirteen-child/images/simmer-header.png'></a></div>";
        } elseif ((is_single && in_category('sweat')) || is_page(889) || is_page(1284) || is_category('sweat')) {
            echo "<div class='category-header'><a href='/sweat'><img src='/wp-content/themes/twentythirteen-child/images/sweat-header.png'></a></div>";
        } elseif ((is_single && in_category('satisfy')) || is_page(885) || is_category('satisfy')) {
            echo "<div class='category-header'><a href='/satisfy'><img src='/wp-content/themes/twentythirteen-child/images/satisfy-header.png'></a></div>";
        } else {
            echo "";
        } ?>

        <div id="submenu">
            <ul class="submenu-menu">
                <li><a href="<?php if (!is_user_logged_in()) {
                        echo "/join";
                    } else {
                        echo "/already-member";
                    } ?>">Join</a></li>
                <li><?php echo do_shortcode('[itroclickpopup id="2"]' . 'Newsletter' . '[/itroclickpopup]'); ?></li>
                <li><a href="/this-weeks-challenges">Challenges</a></li>
                <li><a href="/community">Community</a></li>
                <li><a href="/meet-the-experts/">Experts</a></li>
                <li><a href="/challenge-board">Challenge Board</a></li>
            </ul>
        </div>
    </div>

    <?php if (!is_user_logged_in()) { ?><a href="/join" class="mobile-join-button">Join!</a><?php } ?>


    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <div id="main" class="site-main">