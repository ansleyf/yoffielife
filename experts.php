<?php
/**
Template Name: Experts
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<div id="experts-categories">
							<a data-modal="arin" href="#" >
								<div class="sub-category-button">
									<span>Blaine Arin</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Arin.png">
								</div>
							</a>
							<a data-modal="baer" href="#" >
								<div class="sub-category-button">
									<span>Rebecca Baer</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Baer.png">
								</div>
							</a>
							<a data-modal="bartels" href="#" >
								<div class="sub-category-button">
									<span>Sara Bartels</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/bartels.png">
								</div>
							</a>
							<a data-modal="bauer" href="#" >
								<div class="sub-category-button">
									<span>Courtney Bauer</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Bauer.png">
								</div>
							</a>
							<a data-modal="candlecafe" href="#" >
								<div class="sub-category-button">
									<span>Candle Cafe Team</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/candlecafe.png">
								</div>
							</a>
							<a data-modal="curtis" href="#" >
								<div class="sub-category-button">
									<span>Christina Curtis</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Curtis.png">
								</div>
							</a>
							<a data-modal="choklin" href="#" >
								<div class="sub-category-button">
									<span>Dmitry R. Choklin</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Choklin.png">
								</div>
							</a>
							<a data-modal="curtis" href="#" >
								<div class="sub-category-button">
									<span>Christina Curtis</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Curtis.png">
								</div>
							</a>
							<a data-modal="decker" href="#" >
								<div class="sub-category-button">
									<span>Jessica Decker</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/decker.jpg">
								</div>
							</a>
							<a data-modal="dina" href="#" >
								<div class="sub-category-button">
									<span>Dina D'Alessandro</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Dina.png">
								</div>
							</a>
							<a data-modal="flores" href="#" >
								<div class="sub-category-button">
									<span>Gina Flores</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Flores.png">
								</div>
							</a>
							<a data-modal="fujimura" href="#" >
								<div class="sub-category-button">
									<span>Judith Fujimura</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Fujimura.png">
								</div>
							</a>
							<a data-modal="hubbard" href="#" >
								<div class="sub-category-button">
									<span>Marissa Hubbard</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Marissa.png">
								</div>
							</a>
							<a data-modal="jaensch" href="#" >
								<div class="sub-category-button">
									<span>Mary Wallace Jaensch</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Jaensch.jpg">
								</div>
							</a>
							<a data-modal="macdonald" href="#" >
								<div class="sub-category-button">
									<span>Diane L. MacDonald</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Diane.jpg">
								</div>
							</a>
							<a data-modal="maples" href="#" >
								<div class="sub-category-button">
									<span>Patricia Maples</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Maples.png">
								</div>
							</a>
							<a data-modal="matthews" href="#" >
								<div class="sub-category-button">
									<span>Leah Matthews</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Matthews.jpg">
								</div>
							</a>
							<a data-modal="molina" href="#" >
								<div class="sub-category-button">
									<span>Amy Schnelle Molina</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Molina.png">
								</div>
							</a>
							<a data-modal="ndoci" href="#" >
								<div class="sub-category-button">
									<span>Bonnie Ndoci</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Ndoci.png">
								</div>
							</a>
							<a data-modal="ranf" href="#" >
								<div class="sub-category-button">
									<span>Collin Ranf</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/ranf.jpg">
								</div>
							</a>	
							<a data-modal="robbins" href="#" >
								<div class="sub-category-button">
									<span>Ola Robbins</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Robbins.png">
								</div>
							</a>
							<a data-modal="shelly" href="#" >
								<div class="sub-category-button">
									<span>Peggy Shelly</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Shelly.png">
								</div>
							</a>
							<a data-modal="sutherland" href="#" >
								<div class="sub-category-button">
									<span>Stephanie Sutherland</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/sutherland.jpg">
								</div>
							</a>
							<a data-modal="west" href="#" >
								<div class="sub-category-button">
									<span>Marcie West</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/West.png">
								</div>
							</a>
						</div>						

						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					
				</article><!-- #post -->
				
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="overlays experts">
		<div class="overlay" id="arin">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 6; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simmer" class="simmer">Simmer</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="baer">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 32; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/satisfy" class="satisfy">Satisfy</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="bartels">
		<a class="overlay-close">X</a>
		<?php $author = 83; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/satisfy" class="satisfy">Satisfy</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="bauer">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 28; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/soul" class="soul">Soul</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="candlecafe">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 363; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simmer" class="simmer">Simmer</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="choklin">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 9; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/sweat" class="sweat">Sweat</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="curtis">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 62; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/spark" class="spark">Spark</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="decker">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 311; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="dina">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 61; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/satisfy" class="satisfy">Satisfy</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="flores">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 85; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="fujimura">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 63; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/spark" class="spark">Spark</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="hubbard">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 80; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simmer" class="simmer">Simmer</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="jaensch">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 349; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="macdonald">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 286; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/spark" class="spark">Spark</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="maples">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 31; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/sweat" class="sweat">Sweat</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="matthews">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 30; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="molina">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 10; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simmer" class="simmer">Simmer</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="ndoci">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 16; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="ranf">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 266; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/sweat" class="sweat">Sweat</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="robbins">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 17; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/soul" class="soul">Soul</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="shelly">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 5; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simmer" class="simmer">Simmer</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
		
		<div class="overlay" id="sutherland">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 334; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/soul" class="soul">Soul</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>

		<div class="overlay" id="west">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 8; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/spark" class="spark">Spark</a></h2>
		<?php
			the_author_meta( 'description', $author); ?>
			
			<a href="#" class="overlay-close">X</a>
			
		<?php
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		<a class="author-button" href="<?php echo get_author_posts_url($author); ?>">View All Posts by <?php the_author_meta( 'first_name', $author); ?></a>
		</div>
	</div><!-- overlays -->
<?php get_footer(); ?>