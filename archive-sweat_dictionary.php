<?php
/*
    Template Name: Sweat Dictionary Archive
*/
?>

<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		
			<header class="archive-header">
				<h1 class="archive-title">Malady Encyclopedia</h1>
			</header><!-- .archive-header -->

	<div id="a-z">
<div id="index-nav-list">
<a class="index-nav" href="#A">A</a><a class="index-nav" href="#B">B</a><a class="index-nav" href="#C">C</a><a class="index-nav" href="#D">D</a><a class="index-nav" href="#E">E</a><a class="index-nav" href="#F">F</a><a class="index-nav" href="#G">G</a><a class="index-nav" href="#H">H</a><a class="index-nav" href="#I">I</a><a class="index-nav" href="#J">J</a><a class="index-nav" href="#K">K</a><a class="index-nav" href="#L">L</a><a class="index-nav" href="#M">M</a><a class="index-nav" href="#N">N</a><a class="index-nav" href="#O">O</a><a class="index-nav" href="#P">P</a><a class="index-nav" href="#Q">Q</a><a class="index-nav" href="#R">R</a><a class="index-nav" href="#S">S</a><a class="index-nav" href="#T">T</a><a class="index-nav" href="#U">U</a><a class="index-nav" href="#V">V</a><a class="index-nav" href="#W">W</a><a class="index-nav" href="#X">X</a><a class="index-nav" href="#Y">Y</a><a class="index-nav" href="#Z">Z</a>
</div>
<?php
   $args = array(
    'post_type' => 'sweat_dictionary', 
     'orderby' => 'title',
     'order' => 'ASC',
     'caller_get_posts' => 1,
     'posts_per_page' => -1,
                 );
query_posts($args);

?>

  <div  class='index-title '>

<?php

if (have_posts()) {
    $curr_letter = '';
    while (have_posts()) {
        the_post();
        $this_letter = strtoupper(substr($post->post_title,0,1));

        if ($this_letter != $curr_letter) {
          echo "<div id='$this_letter' class='index-letter'><a name='$this_letter'></a>$this_letter<a href='#top' class='backtotop'>Back to Top</a></div><ul class='triple-grid'>";
          $curr_letter = $this_letter;
        }


    ?>

      
	<li id="$this_letter" class="" ><div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
				</div>
				<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?> 
	</div></li>
	
    <?php 
    
    	 if ($this_letter != $curr_letter) {
          echo "</ul>";
          $curr_letter = $this_letter;
        }

    }
}
?>

</div>
     	</div><!-- End id='a-z' -->
			
			<?php twentythirteen_paging_nav(); ?>


		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>