<?php
	define('WP_USE_THEMES', false);
	require('../../../wp-load.php');

	if (isset($_GET["filters_array"])) {
		$selected_filters = $_GET["filters_array"];
	} elseif (isset($_POST["filters_array"])) {
		$selected_filters = $_POST["filters_array"];
	} else {
		$selected_filters = false;
	}

	if (isset($_GET["keywords_array"])) {
		$selected_keywords = $_GET["keywords_array"];
	} elseif (isset($_POST["filters_array"])) {
		$selected_keywords = $_POST["keywords_array"];
	} else {
		$selected_keywords = false;
	}
?>

<?php /* The loop */
	$filters_array = explode(",", $selected_filters);
	$keywords_array = explode(",", $selected_keywords);
	if ($selected_filters && $selected_keywords) {
		$args=array('post_type' => 'Exercises', 'orderby' => 'name', 'order' => 'ASC', 'tag_slug__and' => $filters_array, 's'=> $selected_keywords);
		$the_query = new WP_Query( $args );
	} elseif ($selected_filters && !$selected_keywords) {
		$args=array('post_type' => 'Exercises', 'orderby' => 'name', 'order' => 'ASC', 'tag_slug__and' => $filters_array);
		$the_query = new WP_Query( $args );
	} elseif (!$selected_filters && $selected_keywords){
		$args=array('post_type' => 'Exercises', 'orderby' => 'name', 'order' => 'ASC', 's'=> $selected_keywords);
		$the_query = new WP_Query( $args );
	} else {
		$the_query = $wp_query;
	}
?>
<ul class="triple-grid">
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<li id="<?php the_ID(); ?>"><div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
	</div>
	<div class="triple-grid-overlay">
	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
	<?php cboard_link() ?> 
	</div>
	</li>

<?php endwhile; ?>
</ul>
<?php twentythirteen_paging_nav(); ?>
<?php
	$wp_query = null;
	$wp_query = $original_query;
	wp_reset_postdata();
?>
