<?php
/**
Template Name: Soul
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area category-page">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php query_posts('category_name=soul&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>"><header class="entry-header">
						<?php if ( ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<div class="title soul">This Week's Soul Challenge</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<?php endif; ?>
						<div class="entry-container">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
							<div class="view soul">View Soul Challenge</div>
						</div>
					</header></a><!-- .entry-header -->

				</article><!-- #post -->

	
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			
			<section class="sub-category-buttons">
				<div class="sub-category-button"><a href="/category/soul/wisdom-insight"><span>Wisdom & Insight</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/wisdom.png"></a></div>
				<div class="sub-category-button"><a href="/category/soul/mindfulness"><span>Mindfulness</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/mindfulness.png"></a></div>
				<div class="sub-category-button"><a href="/category/soul/creativity"><span>Creativity</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/creativity.png"></a></div>
				<div class="sub-category-button"><a href="/category/soul/self-realization"><span>Self-Realization</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/self-realization.png"></a></div>
				<div class="sub-category-button"><a href="/category/soul/gratitude"><span>Gratitude</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/gratitude.png"></a></div>
				<div class="sub-category-button"><a href="/category/soul/rest-relaxation"><span>Rest & Relaxation</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/soul/rest.png"></a></div>
			
			</section>

			<header class="interior-header">
				<h1>Recent Soul Challenges</h1>
			</header>
			<?php query_posts('category_name=soul&showposts=15&offset=1'); ?>
			<?php get_template_part( 'triple-grid' ); ?>
			<?php wp_reset_query(); ?>
			
			<nav class="navigation paging-navigation" role="navigation">
				<h1 class="screen-reader-text">Posts navigation</h1>
				<div class="nav-links">

								<div class="nav-previous"><a href="http://yoffielife.com/category/soul/page/2/"><span class="meta-nav">←</span> Previous Challenges</a></div>
			
			
				</div><!-- .nav-links -->
			</nav>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>