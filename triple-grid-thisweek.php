<ul class="triple-grid">
	<li class="satisfy">
		<div class="category-label">Satisfy Challenge</div>
		<?php
			global $do_not_duplicate;
			$satisfy = array(
				"post_type" => array("post","satisfy_dictionary"),
				"category_name" => "satisfy",
				"showposts" => 1,
				"cat" => 210
			);
			query_posts($satisfy);
			while ( have_posts() ) : the_post(); ?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php if (has_post_thumbnail()) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?>
				</a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="simmer">
		<div class="category-label">Simmer Challenge</div>
		<?php 
			$simmer_dictionary = array(
				"post_type" => array("post","simmer_dictionary"),
				"category_name" => "simmer",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($simmer_dictionary);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="simplify">
		<div class="category-label">Simplify Challenge</div>
		<?php
			$simplify = array(
				"category_name" => "simplify",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($simplify);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="soul">
		<div class="category-label">Soul Challenge</div>
		<?php 
			$soul = array(
				"category_name" => "soul",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($soul);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="spark">
		<div class="category-label">Spark Challenge</div>
		<?php
			$spark = array(
				"category_name" => "spark",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($spark);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="sweat">
		<div class="category-label">Sweat Challenge</div>
		<?php 
			$exercises =array(
				"post_type" => array("post", "exercises", "sweat_dictionary"),
				"category_name" => "sweat",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($exercises);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="scoop">
		<div class="category-label">Scoop</div>
		<?php 
			$exercises =array(
				"category_name" => "yoffie-scoop",
				"cat" => 210,
				"showposts" => 1
			);
			query_posts($exercises);
			while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
		?>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		<?php endwhile; wp_reset_query(); ?>
	</li>
	<li class="yoffie-blog">
		<div class="category-label">Victoria's Blog</div>
		<div class="triple-thumbnail">
			<a href="http://yoffielife.com/category/letters-to-the-reader/" rel="bookmark"><img src="http://yoffielife.com/wp-content/themes/twentythirteen-child/images/yoffie-blog_front.jpg" /></a>
		</div>
		<div class="triple-grid-overlay">
			<a href="http://yoffielife.com/category/letters-to-the-reader/" rel="bookmark">Letters to the Reader</a>
		</div>
	</li>
	<li class="challenge-board-box">
		<div class="category-label">Challenge Board</div>
		<div class="triple-thumbnail">
			<a href="http://yoffielife.com/challenge-board/" rel="bookmark"><img src="http://yoffielife.com/wp-content/themes/twentythirteen-child/images/challenge-board_front.jpg" /></a>
		</div>
		<div class="triple-grid-overlay">
			<a href="http://yoffielife.com/challenge-board/" rel="bookmark">Save challenges and track your progress</a>
		</div>
	</li>
</ul>