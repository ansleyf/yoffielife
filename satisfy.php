<?php
/**
Template Name: Satisfy
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area category-page">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php query_posts('category_name=satisfy&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>"><header class="entry-header">
						<?php if ( ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<div class="title satisfy">This Week's Satisfy Challenge</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<?php endif; ?>
						<div class="entry-container">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
							<div class="view satisfy">View Satisfy Challenge</div>
						</div>
					</header></a><!-- .entry-header -->

				</article><!-- #post -->

	
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			
			<section class="sub-category-buttons">
				<div class="sub-category-button"><a href="/category/satisfy/eat"><span>Eat</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/eat.png"></a></div>
				<div class="sub-category-button"><a href="/category/satisfy/nourish"><span>Nourish</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/nourish.png"></a></div>
				<div class="sub-category-button"><a href="/category/satisfy/balance"><span>Balance</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/balance.png"></a></div>
				<div class="sub-category-button"><a href="/category/satisfy/crowd-out"><span>Crowd Out</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/crowd-out.png"></a></div>
				<div class="sub-category-button"><a href="/category/satisfy/tips-techniques"><span>Tips & Techniques</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/tips.png"></a></div>
				<div class="sub-category-button"><a href="/satisfy-dictionary"><span>Food Encyclopedia</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/dictionary.png"></a></div>
			</section>

			<header class="interior-header">
				<h1>Recent Satisfy Challenges</h1>
			</header>
			
			<?php query_posts('category_name=satisfy&showposts=15&offset=1'); ?>
			<?php get_template_part( 'triple-grid' ); ?>
			<?php wp_reset_query(); ?>
			
			<nav class="navigation paging-navigation" role="navigation">
				<h1 class="screen-reader-text">Posts navigation</h1>
				<div class="nav-links">

								<div class="nav-previous"><a href="http://yoffielife.com/category/satisfy/page/2/"><span class="meta-nav">←</span> Previous Challenges</a></div>
			
			
				</div><!-- .nav-links -->
			</nav>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>