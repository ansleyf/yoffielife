<?php
/**
 * The template for displaying Author bios
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<div class="author-info">
	<div class="author-avatar">
		<?php
		/**
		 * Filter the author bio avatar size.
		 *
		 * @since Twenty Thirteen 1.0
		 *
		 * @param int $size The avatar height and width size in pixels.
		 */
		// $author_bio_avatar_size = apply_filters( 'twentythirteen_author_bio_avatar_size', 74 );
		// echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
		$author = get_user_by("slug", get_query_var("author_name"));
		$user_id = $author->ID;
		echo bp_core_fetch_avatar(array('item_id' => $user_id, 'type' => 'full'));
		?>
		<div class="author-social">
		<?php
			the_author_social($user_id, "website");
			the_author_social($user_id, "website2");
			the_author_social($user_id, "facebook");
			the_author_social($user_id, "twitter");
			the_author_social($user_id, "instagram");
			the_author_social($user_id, "pinterest");
			the_author_social($user_id, "linkedin");
		?>
		</div>
	</div><!-- .author-avatar -->
	<div class="author-description">
		
		<?php the_author_meta( 'description' ); ?>
		<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
			All Challenges by <?php echo get_the_author(); ?>
		</a>
	</div><!-- .author-description -->
	<div class="button-holder">
		<a class="expandbio">View Expanded Bio</a>
		<a class="expandbio hidden close">Close Expanded Bio</a>
	</div>
	<script>
		$('.expandbio').click(function(event) {
			event.preventDefault();
			$('.author-description').toggleClass('open');
			$('.expandbio').toggleClass('hidden');
		});
	</script>
</div><!-- .author-info -->