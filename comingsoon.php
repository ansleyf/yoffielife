<div id="comingsoon-overlay"></div>
<div id="comingsoon">
	<h1 style="text-align: center;">WELCOME TO YOFFIE LIFE<br/>COMING SOON!</h1>
	<div id="login-page-form" class="comingsoon-login"><?php wp_login_form(); ?></div>
</div>
<script>
	$(function(){
		$('html, body').css({
        	'overflow': 'hidden'
    	});
	});
</script>