<?php

	// START Challenge Board
	include("cBoard.Class.php");
	$cboard = new challengeboard;
	include("cBoard.Functions.php");
	// END Challenge Board

/*Add Your Custom Functions Here*/
add_image_size( 'category-feature', 790, 390, true ); 
add_image_size( 'post-featured', 790, 350, true );
add_image_size( 'excerpt-thumb', 255, 255, true );
add_image_size( 'triple-grid', 315, 423, true );
register_sidebar( array(
    'name'         => __( 'Challenge Board' ),
    'id'           => 'sidebar-cboard',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
) );

add_action( 'init', 'sts_post_type' );
function sts_post_type() {
    $labels = array(
            'name' => 'Workout',
            'singular_name' => 'Workout',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Workout',
            'edit_item' => 'Edit Workout',
            'new_item' => 'New Workout',
            'all_items' => 'All Workouts',
            'view_item' => 'View Workout',
            'search_items' => 'Search Workouts',
            'not_found' => 'No workouts found',
            'not_found_in_trash' => 'No workouts found in Trash',
            'parent_item_colon' => '',
            'menu_name' => 'Workouts'
    );

    $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title', 'author', 'thumbnail', 'revisions')
    );

    register_post_type('workout', $args);

    $labels = array(
            'name' => _x('Workout Categories', 'taxonomy general name'),
            'singular_name' => _x('Workout Category', 'taxonomy singular name'),
            'search_items' => __('Search Categories'),
            'all_items' => __('All Categories'),
            'parent_item' => __('Parent Category'),
            'parent_item_colon' => __('Parent Category:'),
            'edit_item' => __('Edit Category'),
            'update_item' => __('Update Category'),
            'add_new_item' => __('Add New Category'),
            'new_item_name' => __('New Category Name'),
            'menu_name' => __('Category'),
    );

    $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'workout-category'),
    );

    register_taxonomy('workout-category', array('workout'), $args);
}

add_action( 'admin_init', 'sts_add_custom_box', 1 );
function sts_add_custom_box() {
    add_meta_box(
            'workout-info',
            __( 'Workout Information' ),
            'sts_workout_meta_box',
            'workout'
    );
}

add_action( 'save_post', 'sts_save_workout_meta_box', 100, 2 );
function sts_save_workout_meta_box( $post_id, $post ) {
    if ( 'workout' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_post', $post_id ) )
            return;

        if ( ! isset( $_POST['review_noncename'] ) || ! wp_verify_nonce( $_POST['review_noncename'], plugin_basename( __FILE__ ) ) )
            return;

        $info = $_POST['_workout_info'];

        $exercises = array();
        $suggested_reps = array();
        $seconds_each = array();
        $suggested_sets = array();

        foreach ($info['exercises'] as $k => $ex) {
            if (!empty($ex)) {
                array_push($exercises, $ex);
                array_push($suggested_reps, $info['suggested_reps'][$k]);
                array_push($seconds_each, $info['seconds_each'][$k]);
                array_push($suggested_sets, $info['suggested_sets'][$k]);
            }
        }

        $info['exercises'] = $exercises;
        $info['suggested_reps'] = $suggested_reps;
        $info['seconds_each'] = $seconds_each;
        $info['suggested_sets'] = $suggested_sets;

        update_post_meta($post_id, '_workout_info', $info);
    }
}

function sts_workout_meta_box( $post ) {
    wp_nonce_field( plugin_basename( __FILE__ ), 'review_noncename' );

    $info = get_post_meta( $post->ID, '_workout_info', true );
    $select2css = get_stylesheet_directory_uri() . '/css/select2.css';
    $select2js = get_stylesheet_directory_uri() . '/js/select2.min.js';

    $exercises = get_posts(array('post_type' => 'exercises', 'posts_per_page' => -1));
    $selects = '';
    $options = '';

    $suggested_reps = $info['suggested_reps'];
    $seconds_each = $info['seconds_each'];
    $suggested_sets = $info['suggested_sets'];

    foreach ($exercises as $ex) {
        $options .= "<option value='$ex->ID'>$ex->post_title</option>";
    }

    if ($info) foreach ($info['exercises'] as $k => $ie) {
        $opts = '';
        foreach ($exercises as $ex) {
            if ($ie == $ex->ID) {
                $opts .= "<option value='$ex->ID' selected>$ex->post_title</option>";
            } else {
                $opts .= "<option value='$ex->ID'>$ex->post_title</option>";
            }
        }

        $selects .=
            '<li class="exercise-wrapper">
                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                <select name="_workout_info[exercises][]" class="select-exercise">
                    <option></option>
                    ' . $opts . '
                </select>

                <div class="exercise-delete">X</div>

                <input name="_workout_info[suggested_reps][]" class="input-text" placeholder="Suggested reps" value="' . $suggested_reps[$k] . '"/>
                <input name="_workout_info[seconds_each][]" class="input-text" placeholder="Seconds each" value="' . $seconds_each[$k] . '"/>
                <input name="_workout_info[suggested_sets][]" class="input-text" placeholder="Suggested sets" value="' . $suggested_sets[$k] . '"/>
            </li>';
    }

    echo
<<<EOF
<table class="workout-table">
    <tr>
        <th><label>Exercises</label></th>
        <td class="exercises">
            <ul>
                <li class="exercise-wrapper exercises-clone" style="display: none;">
                    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                    <select name="_workout_info[exercises][]">
                        <option></option>
                        $options
                    </select>

                    <div class="exercise-delete">X</div>

                    <input name="_workout_info[suggested_reps][]" class="input-text" placeholder="Suggested reps"/>
                    <input name="_workout_info[seconds_each][]" class="input-text" placeholder="Seconds each"/>
                    <input name="_workout_info[suggested_sets][]" class="input-text" placeholder="Suggested sets"/>
                </li>

                $selects

                <li class="exercise-wrapper">
                    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                    <select name="_workout_info[exercises][]" class="select-exercise">
                        <option></option>
                        $options
                    </select>

                    <div class="exercise-delete">X</div>

                    <input name="_workout_info[suggested_reps][]" class="input-text" placeholder="Suggested reps"/>
                    <input name="_workout_info[seconds_each][]" class="input-text" placeholder="Seconds each"/>
                    <input name="_workout_info[suggested_sets][]" class="input-text" placeholder="Suggested sets"/>
                </li>
            </ul>

            <div class="add-exercise-wrapper">
                <input type="button" class="button" id="add-exercise" value="Add More Exercise"/>
            </div>
        </td>
    </tr>
</table>

<link href="$select2css" rel="stylesheet"/>
<script src="$select2js"></script>
<script>
    jQuery(function ($) {
        $('.select-exercise').select2({placeholder: "Select an Exercise"});

        $('#add-exercise').click(function (e) {
            e.preventDefault();

            $('.exercises-clone').clone()
                .removeClass('exercises-clone').show()
                .appendTo('.exercises ul')
                .find('select').addClass('select-exercise').select2({placeholder: "Select an Exercise"});
        })

        $('.exercises').on('click', '.exercise-delete', function (e) {
            e.preventDefault();

            $(this).parent().remove();
        })

        $('.exercises ul').sortable();
    })
</script>
<style>
.exercises ul {
    list-style: none;
    margin: 0;
    padding: 0;
}

.input-text {
    width: 100%;
    border: 1px solid #ddd;
    box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
    background-color: #fff;
    color: #333;
    -webkit-transition: .05s border-color ease-in-out;
    transition: .05s border-color ease-in-out;
}

.exercise-wrapper {
    padding-right: 30px;
    padding-left: 20px;
    position: relative;
}

.exercise-wrapper .ui-icon {
    position: absolute;
    top: 5px;
    left: 0;
    cursor: move;
}

.exercise-delete {
    position: absolute;
    top: 5px;
    right: 5px;
    color: red;
    cursor: pointer;
}

.select-exercise {
    width: 100%;
    margin-bottom: 5px;
}

.workout-table {
    width: 100%;
}

.workout-table th {
    width: 140px;
    font-weight: bold;
    text-align: left;
}

.workout-table td {
    padding: 10px;
}
</style>
EOF;
}

//Adds gallery shortcode defaults of size="excerpt-thumb" and columns="3" 
function amethyst_gallery_atts( $out, $pairs, $atts ) {
   
    $atts = shortcode_atts( array(
        
        'size' => 'excerpt-thumb',
         ), $atts );

    
    $out['size'] = $atts['size'];

    return $out;

}
add_filter( 'shortcode_atts_gallery', 'amethyst_gallery_atts', 10, 3 );

function register_my_menus() {
  register_nav_menus(
    array(
      'footer-menu' => __( 'Footer Menu' ),
      'disclaimer-menu' => __( 'Disclaimer Menu' ),
      'cuisine-menu' => __( 'Cuisine Menu' ),
      'meal-menu' => __( 'Meal Menu' ),
      'sweat-menu' => __( 'Sweat Menu' ),
      'mobile-menu' => __( 'Mobile Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Remove post formats support
add_action('after_setup_theme', 'remove_post_formats', 11);
function remove_post_formats() {
    remove_theme_support('post-formats');
}

if ( ! function_exists( 'twentythirteen_paging_nav' ) ) :
/**
 * Displays navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentythirteen' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous Challenges', 'twentythirteen' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'More Challenges <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'twentythirteen_post_nav' ) ) :
/**
 * Displays navigation to next/previous post when applicable.
*
* @since Twenty Thirteen 1.0
*
* @return void
*/
function twentythirteen_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'twentythirteen' ); ?></h1>
		<div class="nav-links">

			<div class="nav-previous"><?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> Previous Challenges', 'Previous post link', 'twentythirteen' ) ); ?></div>
			<div class="nav-next"><?php next_post_link( '%link', _x( 'More Challenges <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen' ) ); ?></div>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'twentythirteen_entry_meta' ) ) :
function twentythirteen_entry_meta() {

	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'twentythirteen' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'page' !== get_post_type() )
		twentythirteen_entry_date();

	

	// Post author
	if ( 'page' !== get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'twentythirteen' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'twentythirteen_cats_tags' ) ) :
function twentythirteen_cats_tags() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'twentythirteen' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}
}
endif;

if ( ! function_exists ( 'do_sidebar_cat_header' ) ) :
function do_sidebar_cat_header() {

	if ( ( is_single && in_category( 'soul' )) || is_page(888) || is_category( 'soul' ) ) { echo "View All Soul Topics"; } elseif ( ( is_single && in_category( 'spark' )) || is_page(890) || is_category( 'spark' ) ) { echo "View All Spark Topics"; } elseif ( ( is_single && in_category( 'simplify' )) || is_page(887) || is_category( 'simplify' ) ) { echo "View All Simplify Topics"; } elseif ( ( is_single && in_category( 'simmer' )) || is_page(886) || is_category( 'simmer' ) ) { echo "View All Simmer Topics"; } elseif ( ( is_single && in_category( 'sweat' )) || is_page(889) || is_category( 'sweat' ) ) { echo "View All Sweat Topics"; } elseif ( ( is_single && in_category( 'satisfy' )) || is_page(885) || is_category( 'satisfy' ) ) { echo "View All Satisfy Topics"; } else { echo "View All Categories"; }

}
endif;

//add CPTs to author bios
function custom_post_author_archive( $query )
{
  if ( is_author() && empty( $query->query_vars['suppress_filters'] ) )
  {
    $query->set( 'post_type', array( 'post', 'sweat_dictionary', 'satisfy_dictionary', 'cooks_encyclopedia' ) );
    return $query;
  }
}
add_filter( 'pre_get_posts', 'custom_post_author_archive' );


if ( ! function_exists( 'do_mailchimp' ) ) :
function do_mailchimp() {

		printf( "<!-- Begin MailChimp Signup Form -->
<div id='mc_embed_signup'>
<form action='http://yoffielife.us3.list-manage1.com/subscribe/post?u=416a6ea71b68048ef15e77308&amp;id=ca69cc781d' method='post' id='mc-embedded-subscribe-form' name='mc-embedded-subscribe-form' class='validate' target='_blank' novalidate>
	
<div class='indicates-required'><span class='asterisk'>*</span> indicates required</div>
<div class='mc-field-group'>
	<label for='mce-EMAIL'>Email Address  <span class='asterisk'>*</span>
</label>
	<input type='email' value='' name='EMAIL' class='required email' id='mce-EMAIL'>
</div>
<div class='mc-field-group'>
	<label for='mce-FNAME'>First Name </label>
	<input type='text' value='' name='FNAME' class='' id='mce-FNAME'>
</div>
<div class='mc-field-group'>
	<label for='mce-LNAME'>Last Name </label>
	<input type='text' value='' name='LNAME' class='' id='mce-LNAME'>
</div>
	<div id='mce-responses' class='clear'>
		<div class='response' id='mce-error-response' style='display:none'></div>
		<div class='response' id='mce-success-response' style='display:none'></div>
	</div>	<div class='clear'><input type='submit' value='Subscribe' name='subscribe' id='mc-embedded-subscribe' class='button'></div>
</form>
</div>

<!--End mc_embed_signup-->");
} 
endif;

function my_connection_types() {
	p2p_register_connection_type( array(
		'name' => 'exercises_to_posts',
		'from' => 'post',
		'to' => 'exercises'
	) );
}
add_action( 'p2p_init', 'my_connection_types' );

function yoffie_ajax() 
{

     wp_localize_script( 'ajax_slider', 'yoffie_ajax_slider', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}

function ShortenText($text) { // Function name ShortenText
  $chars_limit = 35; // Character length
  $chars_text = strlen($text);
  $text = $text." ";
  $text = substr($text,0,$chars_limit);
  $text = substr($text,0,strrpos($text,' '));

  if ($chars_text > $chars_limit)
     { $text = $text."..."; } // Ellipsis
     return $text;
}

function ShortenText55($text) { // Function name ShortenText
  $chars_limit = 55; // Character length
  $chars_text = strlen($text);
  $text = $text." ";
  $text = substr($text,0,$chars_limit);
  $text = substr($text,0,strrpos($text,' '));

  if ($chars_text > $chars_limit)
     { $text = $text."..."; } // Ellipsis
     return $text;
}

function my_login_logo() { ?>
    <style type="text/css">
	body.login {
		background: #000;
		color: #fff;
	}
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo-white.png);
            padding-bottom: 30px;
	    width: 320px;
	    background-size: 320px;
		height: 110px;
        }
	#login_error {
		color: #141412;
	}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Add Shortcode
function shadow_box( $atts , $content = null ) {

	// Code
return '<div class="shadow-box">' . $content . '</div>';

}
add_shortcode( 'box', 'shadow_box' );

add_action('admin_head', 'expose_mask_hide');

function expose_mask_hide() {
  echo '<style>
    #exposeMask {
	display: none!important;
    } 
  </style>';
}

function replace_howdy( $wp_admin_bar ) {
 $my_account=$wp_admin_bar->get_node('my-account');
 $newtitle = str_replace( 'Howdy,', 'Make a Change Today, ', $my_account->title );
 $wp_admin_bar->add_node( array(
 'id' => 'my-account',
 'title' => $newtitle,
 ) );
 }
 add_filter( 'admin_bar_menu', 'replace_howdy',25 );

//disable WordPress sanitization to allow more than just $allowedtags from /wp-includes/kses.php
remove_filter('pre_user_description', 'wp_filter_kses');
//add sanitization for WordPress posts
add_filter( 'pre_user_description', 'wp_filter_post_kses');

// Makes sure that collapse-o-matic javascript is not loaded on Muut forum pages.
function yoffielife_custom_disable_collapsomatic_for_muut() {
	global $post;
	if ( isset( $post ) && $post->post_type == 'page' && ( has_shortcode( $post->post_content, 'muut' ) || has_shortcode( $post->post_content, 'moot' ) ) ) {
		wp_dequeue_script( 'collapseomatic-js' );
		wp_dequeue_style( 'collapseomatic-css' );
	}
}
add_action('wp_enqueue_scripts', 'yoffielife_custom_disable_collapsomatic_for_muut', 50 );


function the_author_social($author, $network = "facebook") {
	$args = array(
		"field" => $network,
		"user_id" => $author
	);
	$url = bp_get_profile_field_data($args);
	// echo $url;
	if(!empty($url)) {
		$html = '<a href="'.$url.'" id="'.$network.'" target="_blank" class="social-icon">';
		$html .= '</a>';
	}
	echo $html;
}


function my_use_muut_comments_template_force( $template ) {
	if ( class_exists( 'Muut_Template_Loader' ) ) {
		$template = Muut_Template_Loader::instance()->locateTemplate( 'comments.php' );
	}
	return $template;
}
add_filter( 'comments_template', 'my_use_muut_comments_template_force' );

function my_load_muut_script_function() {
	if ( class_exists( 'Muut' ) && is_singular() && comments_open() ) {
		add_filter( 'muut_is_muut_post', '__return_true' );
	}
}
add_action( 'wp', 'my_load_muut_script_function' );

?>