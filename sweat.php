<?php
/**
Template Name: Sweat
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area category-page">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php $exercises =array(
						"post_type" => array("post", "exercises", "sweat_dictionary"),
						"category_name" => "sweat",
						"cat" => 210,
						"showposts" => 1
					);
			query_posts($exercises); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>"><header class="entry-header">
						<?php if ( ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<div class="title sweat">This Week's Sweat Challenge</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<?php endif; ?>
						<div class="entry-container">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
							<div class="view sweat">View Sweat Challenge</div>
						</div>
					</header></a><!-- .entry-header -->

				</article><!-- #post -->

	
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			
			<section class="sub-category-buttons">
				<div class="sub-category-button"><a href="/exercises"><span>Exercises</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/exercises.png"></a></div>
				<div class="sub-category-button"><a href="/category/sweat/workouts"><span>Expert Workouts</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/workouts.png"></a></div>
				<div class="sub-category-button"><a href="/your-workouts"><span>Build Your Workout</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/your-workouts.png"></a></div>
				<div class="sub-category-button"><a href="/category/sweat/sweat-tips"><span>Tips & Techniques</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/tips.png"></a></div>
				<div class="sub-category-button"><a href="/category/sweat/integrative-therapy"><span>Integrative Therapy</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/therapy.png"></a></div>
				<div class="sub-category-button"><a href="/sweat-dictionary"><span style="font-size: .94em">Malady Encyclopedia</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sweat/injuries.png"></a></div>
				
			</section>

			<header class="interior-header">
				<h1>Recent Sweat Challenges</h1>
			</header>
			<?php query_posts('category_name=sweat&showposts=15&offset=1'); ?>
			<?php get_template_part( 'triple-grid' ); ?>
			<?php wp_reset_query(); ?>
			
			<nav class="navigation paging-navigation" role="navigation">
				<h1 class="screen-reader-text">Posts navigation</h1>
				<div class="nav-links">

								<div class="nav-previous"><a href="http://yoffielife.com/category/sweat/page/2/"><span class="meta-nav">←</span> Previous Challenges</a></div>
			
			
				</div><!-- .nav-links -->
			</nav>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>