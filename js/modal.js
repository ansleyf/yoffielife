$(function() {
	$('#loading').fadeOut(10);
	$('#tabbed-nav').show(1000);
	$('#logintrigger').click(function(event) {
		event.preventDefault();
		$('#fade').fadeIn();
		$('#loginpopup').fadeIn();
		$('html, body').css({
			'overflow': 'hidden',
			'height': '100%'
		});
	});
	$('#loginpopup a').click(function(event) {
		event.preventDefault();
		$('#fade').fadeOut();
		$('#loginpopup').fadeOut();
		$('html, body').css({
			'overflow': 'auto',
			'height': 'auto'
		});
	});
	$('#experts-categories a:not(.overlay-close)').click(function(event) {
		event.preventDefault();
		id = $(this).attr('data-modal');
		$('#fade').fadeIn();
		$('.overlays #'+id).fadeIn();
		$('html, body').css({
			'overflow': 'hidden',
			'height': '100%'
		});
	});
	$('#services-categories a:not(.overlay-close)').click(function(event) {
		event.preventDefault();
		id = $(this).attr('data-modal');
		$('#fade').fadeIn();
		$('.overlays #'+id).fadeIn();
		$('html, body').css({
			'overflow': 'hidden',
			'height': '100%'
		});
	});
	$('.overlay-close').click(function(event) {
		event.preventDefault();
		$('#fade').fadeOut();
		$('#'+id+'.overlay').fadeOut();
		$('html, body').css({
			'overflow': 'auto',
			'height': 'auto'
		});
	});
	$('#fade').click(function(event) {
		event.preventDefault();
		$('#fade').fadeOut();
		$('#'+id+'.overlay').fadeOut();
		$('html, body').css({
			'overflow': 'auto',
			'height': 'auto'
		});
	});
});