<?php
/*
Template Name: This Weeks Challenges
*/

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

			<div class="entry-content">
			<?php the_content(); ?>
			<?php get_template_part( 'triple', 'grid-thisweek' ); ?>
			<?php endwhile; ?>
			
			

		</div><!-- .entry-content -->

		</article><!-- #post -->
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>