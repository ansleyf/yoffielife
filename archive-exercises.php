<?php
/*
Template Name: Exercise Archive
*/
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php get_sidebar(); ?>
<?php
// Setting the tag categories here. Yes, this makes me sad too. Sorry, theres nothing I can do about it.
// format: slug => tag
$body_parts = array(
	'abs' => 'Abs',
	'ankles' => 'Ankles',
	'arms' => 'Arms',
	'back' => 'Back',
	'biceps' => 'Biceps',
	'butt' => 'Butt',
	'calves' => 'Calves',
	'chest' => 'Chest',
	'core' => 'Core',
	'feet' => 'Feet',
	'forearms' => 'Forearms',
	'full-body' => 'Full Body',
	'glutes' => 'Glutes',
	'groin' => 'Groin',
	'hamstrings' => 'Hamstrings',
	'handsfingers' => 'Hands/Fingers',
	'hips' => 'Hips',
	'hip-flexors' => 'Hip Flexors',
	'inner-thighs' => 'Inner Thighs',
	'knees' => 'Knees',
	'lats' => 'Lats',
	'legs' => 'Legs',
	'lower-back' => 'Lower Back',
	'lower-body' => 'Lower Body',
	'neckjaw' => 'Neck/Jaw',
	'obliques' => 'Obliques',
	'outer-thighs' => 'Outer Thighs',
	'quads' => 'Quads',
	'rhomboids' => 'Rhomboids',
	'shoulders' => 'Shoulders',
	'spine' => 'Spine',
	'traps' => 'Traps',
	'triceps' => 'Triceps',
	'upper-body' => 'Upper Body',
	'wrists' => 'Wrists'
	);
// There are more in this next one but I stopped for time
$methods = array(
	'aerobics' => 'Aerobics',
	'anya' => 'ANYA',
	'ballet' => 'Ballet',
	'chair-fitness' => 'Chair Fitness',
	'core-trainings' => 'Core Trainings',
	'dance' => 'Dance',
	'healing' => 'Healing',
	'integrative-therapy' => 'Integrative Therapy',
	'physical-therapy' => 'Physical Therapy',
	'pilates' => 'Pilates',
	'plyometrics' => 'Plyometrics',
	'restorative-yoga' => 'Restorative Yoga',
	'self-massage' => 'Self-Massage',
	'strength-training' => 'Strength Training',
	'stretching' => 'Stretching',
	'weight-training' => 'Weight Training',
	'yoffie-method' => 'Yoffie Method',
	'yoga' => 'Yoga'
	);

$goals = array(
	'balance-stability' => 'Balance &amp; Stability',
	'calm' => 'Calm',
	'endurance' => 'Endurance',
	'energy' => 'Energy',
	'flexibility' => 'Flexibility',
	'heart-health' => 'Heart Health',
	'mental-health' => 'Mental Health',
	'mobility' => 'Mobility',
	'pain-relief' => 'Pain Relief',
	'prepost-natal' => 'Pre/Post Natal',
	'power' => 'Power',
	'rehab' => 'Rehab',
	'rest-restore' => 'Rest &amp; Restore',
	'speed' => 'Speed',
	'sports-conditioning' => 'Sports Conditioning',
	'stress-management' => 'Stress Management',
	'strength' => 'Strength',
	'tone' => 'Tone',
	'weight-loss' => 'Weight Loss'
	);

$pain_relief = array(
	'ankles' => 'Ankles',
	'back' => 'Back',
	'butt' => 'Butt',
	'calves' => 'Calves',
	'chest' => 'Chest',
	'feettoes' => 'Feet/Toes',
	'forearms' => 'Forearms',
	'glutes' => 'Glutes',
	'groin' => 'Groin',
	'hamstrings' => 'Hamstrings',
	'handsfingers' => 'Hands/Fingers',
	'hips' => 'Hips',
	'hip-flexors' => 'Hip Flexors',
	'inner-thighs' => 'Inner Thighs',
	'knees' => 'Knees',
	'legs' => 'Legs',
	'low-back' => 'Low Back',
	'lower-body' => 'Lower Body',
	'neckjaw' => 'Neck/Jaw',
	'outer-thighs' => 'Outer Thighs',
	'quads' => 'Quads',
	'shoulders' => 'Shoulders',
	'spine' => 'Spine',
	'stomach' => 'Stomach',
	'wrists' => 'Wrists'
	);

?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_post_type_archive( 'exercises' ) ) :
						_e( 'Exercises', 'twentythirteen' );
					elseif ( is_post_type_archive( 'simmer-dictionary' ) ) :
						_e( 'Simmer Dictionary', 'twentythirteen' );
					elseif ( is_post_type_archive( 'sweat-dictionary' ) ) :
						_e( 'Sweat Dictionary', 'twentythirteen' );
					else :
						_e( 'Archives', 'twentythirteen' );
					endif;
				?></h1>
			</header><!-- .archive-header -->
			<div id="filtered-search">
				<form id='filter-options'>
					<h3>Body Part</h3>
						<ul>
							<?php
								foreach ($body_parts as $key => $value) {
									$html = "<li><input type='checkbox' value='$key' class='filters'>";
									$html .= "$value </input>";
									echo $html;
									echo "</li>";
								}
							?>
						</ul>
					<h3>Methods</h3>
						<ul>
							<?php
								foreach ($methods as $key => $value) {
									$html = "<li><input type='checkbox' value='$key' class='filters'>";
									$html .= "$value </input>";
									echo $html;
									echo "</li>";
								}
							?>
						</ul>
					<h3>Goals</h3>
						<ul>
							<?php
								foreach ($goals as $key => $value) {
									$html = "<li><input type='checkbox' value='$key' class='filters'>";
									$html .= "$value </input>";
									echo $html;
									echo "</li>";
								}
							?>
						</ul>
					<h3>Pain Relief</h3>
						<ul>
							<?php
								foreach ($pain_relief as $key => $value) {
									$html = "<li><input type='checkbox' value='$key' class='filters'>";
									$html .= "$value </input>";
									echo $html;
									echo "</li>";
								}
							?>
						</ul>
					<h3>Keyword Search</h3>
						<div id='keyword-search-container'>
							<input type='text' id='exercise-keywords-search'></input>
						</div>
					<a href="<?php bloginfo('wpurl'); ?>/exercises">Clear Filters</a>
				</form>
				<button name="apply" id='filter-button'>Filter</button>
			</div><!-- filtered search -->
			<div id='show-filters'><a href='#' id='show-filters-text'>Show Filters</a></div>
			<div id='ajaxcontent'>
				<ul class="triple-grid">
					<?php while ( have_posts() ) : the_post(); ?>
						<li id="<?php the_ID(); ?>"><div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
						</div>
						<div class="triple-grid-overlay">
						<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
						<?php cboard_link() ?> 
						</div>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<!-- Javascipt function to get filters and display them -->
<script>
var appliedFilters = [];
var searchKeywords = [];
var filtersShown = false
$( '#filtered-search' ).hide(0);
$( '#show-filters' ).click(function(){
	if (filtersShown) {
		$( '#filtered-search' ).hide(400);
		$( '#show-filters-text' ).html('Show Filters');
		filtersShown = false;
	} else {
		$( '#filtered-search' ).show(400);
		$( '#show-filters-text' ).html('Hide Filters');
		filtersShown = true;
	}
});
$("form[id=filter-options] input:checkbox").click(function() {
	appliedFilters = $('input:checkbox:checked.filters').map(function () {
	  return this.value;
	}).get();
});
$( '#filter-button' ).click(function(event){
	event.preventDefault();
	$('input:text[id=exercise-keywords-search]').map(function () {
	  var searchString = this.value;
	  searchString = $.trim(searchString);
	  searchKeywords = searchString.split(" ");
	}).get();
	$.ajax({
		// url: "<?php bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php",
		url: "<?php bloginfo('wpurl'); ?>/wp-content/themes/twentythirteen-child/exercises-filter.php",
		type: "POST",
		data: "action=set_tags_to_filter&filters_array=" + appliedFilters + "&keywords_array=" + searchKeywords,
		success: function(results) {
			// alert(results);
			$( '#ajaxcontent' ).empty();
			$( '#ajaxcontent' ).html(results);
			$( '.nav-previous' ).hide();
		}
	});
});
</script>
<?php get_footer(); ?>
