<?php
	class challengeboard 
	{
		public $currusr = 0; // Current User
		public $pod = null; // Challenge Board POD data
		public $alltime = 0; // Challenge Board All Time
		public $mgoalt = 0; // Challenge Board Goal Target
		public $mgoaln = 0; // Challenge Board Next Goal Target
		public $mgoalc = 0; // Challenge Board Goal Progress

		function __construct() {
			$this->currusr = get_current_user_id();
			
			$this->alltime = get_user_meta($this->currusr, "cboard_alltime", true);
			if(empty($this->alltime)) $this->alltime = 0;

			$this->mgoalt = get_user_meta($this->currusr, "cboard_monthlygoal", true);
			if(empty($this->mgoalt) || $this->mgoalt == 0) $this->mgoalt = "Not Set";

			$this->mgoalc = get_user_meta($this->currusr, "cboard_monthlyscore", true);
			if(empty($this->mgoalc) || $this->mgoalt == "Not Set") $this->mgoalc = 0;

			$this->mgoaln = get_user_meta($this->currusr, "cboard_monthlynewgoal", true);
			if(empty($this->mgoaln)) $this->mgoaln = $this->mgoalt;

			$this->pod = pods("challengeboard");

			if($this->alltime == 0 && $this->mgoalt !== "Not Set") $this->init_point_update();
			if(date("d") == "01") $this->monthly_reset();
		}

		private function init_point_update() {
			$update = $this->get_completed();
			$currmnyr = date("m-Y");
			while($update->fetch()) {
				$worth = $this->get_post_points($update->raw("chal"));
				$post_modified = new datetime($update->raw("post_modified"));
				$post_modified = $post_modified->format("m-Y");
				$this->alltime += $worth;
				$this->badge_check();
				update_user_meta($this->currusr, "cboard_alltime", $this->alltime);
				if($currmnyr == $post_modified) $this->mgoalc += $worth;
				update_user_meta($this->currusr, "cboard_monthlyscore", $this->mgoalc);
			}
		}

		private function monthly_reset() {
			$users = pods("user")->find(array("limit" => -1, "where" => "cboard_monthlyscore.meta_value > 0"));
			while($users->fetch()) {
				$this->currusr = $users->raw("ID");
				$this->mgoalt = $users->raw("cboard_monthlygoal");
				$this->mgoalc = $users->raw("cboard_monthlyscore");
				$this->alltime = $users->raw("cboard_alltime");
				$this->mgoaln = $users->raw("cboard_monthlynewgoal");
				$this->goal_check();
				if($this->mgoalc > $this->mgoalt) {
					$diff = $this->mgoalc - $this->mgoalt;
					$this->alltime += $diff;
					$data["cboard_alltime"] = $this->alltime;
				}
				if(is_numeric($this->mgoaln) && $this->mgoaln !== $this->mgoalt) $data["cboard_monthlygoal"] = $this->mgoaln;
				$data["cboard_monthlyscore"] = 0;
				$data["cboard_goalcomplete"] = "false";
				$users->save($data);
				unset($data);
			}
		}

		private function goal_check() {
			if($this->mgoalc >= $this->mgoalt && get_user_meta($this->currusr, "cboard_goalcomplete", true) !== "true") {
				$this->alltime += $this->mgoalt;
				update_user_meta($this->currusr, "cboard_alltime", $this->alltime);
				update_user_meta($this->currusr, "cboard_goalcomplete", "true");
				$this->badge_check();
				return $this->alltime;
			}
			return false;
		}

		private function badge_check() {
			// Eventually this will be dynamic. (maybe)
			if($this->alltime >= 10) $achievement_ids[] = 10478; // Explorer
			if($this->alltime >= 50) $achievement_ids[] = 10474; // Devotee
			if($this->alltime >= 150) $achievement_ids[] = 10472; // Awakened
			if($this->alltime >= 300) $achievement_ids[] = 10482; // Warrior
			if($this->alltime >= 700) $achievement_ids[] = 10480; // Master
			if($this->alltime >= 1500) $achievement_ids[] = 10476; // Enlightened
			if(isset($achievement_ids)) {
				$has_earned = badgeos_get_user_earned_achievement_ids($this->currusr);
				foreach ($achievement_ids as $achievement_id) {
					if(!in_array($achievement_id, $has_earned)) {
						$return[] = badgeos_award_achievement_to_user($achievement_id, $this->currusr);
					}
				}
				return $return;
			}
			return false;
		}

		public function get_badges() {
			return badgeos_get_user_earned_achievement_ids($this->currusr);
		}

		public function the_badges() {
			foreach ($this->get_badges() as $badge) {
				echo badgeos_get_achievement_post_thumbnail($badge, array(100,100));
			}
		}

		private function null_user($userid) {
			if(is_null($userid)) {
				return $this->currusr;
			} else {
				return $userid;
			}
		}

		public function get_post_points($postid = null) {
			$points = get_post_meta($this->null_postid($postid), "cboard_points", true);
			if(!is_numeric($points)) {
				$points = 1;
			}
			return $points;
		}

		public function null_postid($postid) {
			if(is_null($postid)) {
				return get_the_id();
			} else {
				return $postid;
			}
		}

		private function reinit() {
			$this->pod = pods("challengeboard");
		}

		private function encode($data) {
			if(!is_array($data)) {
				$data = array("html" => $data);
			}
			return json_encode($data);
		}

		private function challenge_exists($postid){
			$params = array(
				"limit" => 1,
				"where" => "user.id = $this->currusr AND d.chal = $postid"
			);
			$pod = pods("challengeboard", $params);
			if(is_array($pod->raw("challenge"))) {
				return $pod->raw("state");
			} else {
				return false;
			}
		}

		public function get_incomplete($userid = null) {
			$userid = $this->null_user($userid);
			$this->reinit();
			$params = array(
				"limit" => -1,
				"where" => "user.id = $userid AND state = 'added'"
			);
			return $this->pod->find($params);
		}

		public function get_completed($userid = null) {
			$userid = $this->null_user($userid);
			$this->reinit();
			$params = array(
				"limit" => -1,
				"where" => "user.id = $userid AND state = 'completed'"
			);
			return $this->pod->find($params);
		}

		public function build_link($action = null, $onboard = false, $postid = null) {
			$postid = $this->null_postid($postid);
			$points = $this->get_post_points($postid);
			if(is_null($action)) {
				if($this->challenge_exists($postid) == false) {
					$action = "add";
				} else {
					if($this->challenge_exists($postid) == "added") {
						$action = "complete";
					} elseif($this->challenge_exists($postid) == "completed") {
						$action = "remove";
					}
				}
			}
			// Add challenge to cBoard
			if($onboard && $action == "add") {
				$link = $this->build_link_html($postid, "add", "Add to Challenge Board", "onboard");
			} elseif(!$onboard && $action == "add") {
				$link = $this->build_link_html($postid, "add", "Add to Challenge Board");
			}
			// Complete challenge on cBoard
			if($onboard && $action == "complete") {
				$link = $this->build_link_html($postid, "complete", "Mark Challenge Complete", "onboard");
			} elseif(!$onboard && $action == "complete") {
				$link = $this->build_link_html($postid, "complete", "Mark Challenge Complete");
			}
			// Remove challenge from cBoard
			if($onboard && $action == "remove") {
				$link = $this->build_link_html($postid, "remove", "Remove from Challenge Board", "onboard");
			} elseif(!$onboard && $action == "remove") {
				$link = $this->build_link_html($postid, "link", "View Challenge Board");
			}
			return $link;
		}

		private function build_link_html($postid, $action, $text, $onboard = "") {
			if($action == "link") {
				$link = get_permalink(1011);
			} else {
				$link = "?cboard=$action&postid=$postid";
			}
			return "<a id='$postid' class='cBoard $action $onboard' href='$link' rel='nofollow'><i class='fa cLoader $action'></i> <span class='text'>$text</span></a>";
		}

		public function add_challenge($postid = null, $onboard = false) {
			$postid = $this->null_postid($postid);
			$data = array(
				"chal" => $postid,
				"state" => "added",
				"user" => $this->currusr,
			);
			$this->pod->add($data);
			echo $this->encode($this->build_link("complete", $onboard == "onboard" ? true : false, $postid));
		}

		public function complete_challenge($postid = null, $onboard = false) {
			$postid = $this->null_postid($postid);
			if(empty($onboard)) $onboard = false;
			$params = array("where" => "user.id = $this->currusr AND d.chal = $postid");
			$this->reinit();
			$save = $this->pod->find($params);
			$data = array(
					"state" => "completed",
					"post_modified" => date("Y-m-d H:i:s")
				);
			$save->save($data);
			unset($data);
			$worth = $this->get_post_points($postid);

			$this->mgoalc += $worth;
			update_user_meta($this->currusr, "cboard_monthlyscore", $this->mgoalc);
			$gc = $this->goal_check();

			$data["html"] = $this->build_link("remove", $onboard == "onboard" ? true : false, $postid);
			if($onboard !== false) $data["mgoalc"] = $this->mgoalc;
			if($gc !== false) $data["alltime"] = $gc;

			echo $this->encode($data);
		}

		public function remove_challenge($postid = null, $onboard = false) {
			$postid = $this->null_postid($postid);
			$params = array("where" => "user.id = $this->currusr AND d.chal = $postid");
			$this->reinit();
			$del = $this->pod->find($params);
			$del->delete();
			echo $this->encode($this->build_link("add", $onboard == "onboard" ? true : false, $postid));
		}

		public function clear_challengeboard() {
			$params = array("where" => "user.id = $this->currusr");
			$this->reinit();
			$clear = $this->pod->find($params);
			while ($clear->fetch()) {
				$clear->delete();
			}
		}

		public function set_monthlygoal($goal) {
			if(is_numeric($this->mgoalt)) {
				update_user_meta($this->currusr, "cboard_monthlynewgoal", $goal);
			} else {
				update_user_meta($this->currusr, "cboard_monthlygoal", $goal);
			}
		}
	}