<?php
/**
Template Name: Guest Contributors
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<div id="experts-categories">
							<a data-modal="fahey" href="#" >
								<div class="sub-category-button">
									<span>Kenny Fahey</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Fahey.png">
								</div>
							</a>
							<a data-modal="mcloughlin" href="#" >
								<div class="sub-category-button">
									<span>Lindsay McLoughlin</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/McLoughlin.jpg">
								</div>
							</a>
							<a data-modal="parrish" href="#" >
								<div class="sub-category-button">
									<span>Rebecca Parrish</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Parrish.png">
								</div>
							</a>
							<a data-modal="pleas" href="#" >
								<div class="sub-category-button">
									<span>Kayleigh Pleas</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/experts/Pleas.png">
								</div>
							</a>
						</div>						

						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					
				</article><!-- #post -->
				
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="overlays experts">

		<div class="overlay" id="fahey">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 27; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author);
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		</div>

		<div class="overlay" id="mcloughlin">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 7; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/simplify" class="simplify">Simplify</a></h2>
		<?php
			the_author_meta( 'description', $author);
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		</div>


		<div class="overlay" id="parrish">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 14; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/spark" class="spark">Spark</a></h2>
		<?php
			the_author_meta( 'description', $author);
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		</div>

		<div class="overlay" id="pleas">
		<a href="#" class="overlay-close">X</a>
		<?php $author = 13; ?>
		<h2><a href="<?php echo get_author_posts_url($author); ?>"><?php the_author_meta( 'display_name', $author); ?></a> <span class="divider">|</span> <a href="/soul" class="soul">Soul</a></h2>
		<?php
			the_author_meta( 'description', $author);
			the_author_social($author, "website");
			the_author_social($author, "website2");
			the_author_social($author);
			the_author_social($author, "twitter");
			the_author_social($author, "instagram");
			the_author_social($author, "pinterest");
			the_author_social($author, "linkedin");
		?>
		</div>

	</div><!-- overlays -->
<?php get_footer(); ?>