<?php
/**
Template Name: Simmer
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area category-page">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php query_posts('category_name=simmer&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<a href="<?php the_permalink(); ?>"><header class="entry-header">
						<?php if ( ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<div class="title simmer">This Week's Simmer Challenge</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<?php endif; ?>
						<div class="entry-container">
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
							<div class="view simmer">View Simmer Challenge</div>
						</div>
					</header></a><!-- .entry-header -->

				</article><!-- #post -->

	
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			
			<section class="sub-category-buttons">
				<div class="sub-category-button"><a href="/category/simmer/fruit"><span>Fruit</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/fruit.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/vegetable"><span>Vegetable</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/vegetables.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/greens"><span>Greens</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/greens.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/whole-grains"><span>Whole Grains</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/whole-grains.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/protein"><span>Protein</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/protein.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/fiber"><span>Fiber</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/fibre.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/treats"><span>Treats</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/treats.png"></a></div>
				<div class="sub-category-button"><a href="/category/simmer/basics"><span>Basics</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/simmer/veggie-protein-2.png"></a></div>
				<div class="sub-category-button"><a href="/simmer-dictionary"><span>Food Encyclopedia</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/satisfy/dictionary.png"></a></div>
			</section>

			<header class="interior-header">
				<h1>Recent Simmer Challenges</h1>
			</header>
			<?php query_posts('category_name=simmer&showposts=15&offset=1'); ?>
			<?php get_template_part( 'triple-grid' ); ?>
			<?php wp_reset_query(); ?>
			
			<nav class="navigation paging-navigation" role="navigation">
				<h1 class="screen-reader-text">Posts navigation</h1>
				<div class="nav-links">

								<div class="nav-previous"><a href="http://yoffielife.com/category/simmer/page/2/"><span class="meta-nav">←</span> Previous Challenges</a></div>
			
			
				</div><!-- .nav-links -->
			</nav>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>