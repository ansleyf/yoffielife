<ul class="triple-grid">
	<?php while ( have_posts() ) : the_post();?>
		<li>
			<div class="triple-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('triple-grid'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/white-y.png">'; } ?></a>
			</div>
			<div class="triple-grid-overlay">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
				<?php cboard_link() ?>
			</div>
		</li>			
	<?php endwhile; ?>    
</ul>