<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="print-only">
	<header class="entry-header">
		
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
			<?php if ( is_search() || is_category()) : ?>
			<div class="entry-thumbnail">
				
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('post-featured'); ?></a>
			</div>
			<?php else : ?>
			<div class="entry-thumbnail">
				<?php get_template_part( 'breadcrumbs'); ?>
				<?php $worth = get_post_points(); ?>
				<div class="post-points">
					<div class="hover" style="display: none;">
						<div class="top">
							This Challenge is Worth...
							<h4><?php echo $worth ?> Life Point<?php if($worth > 1) echo "s"; ?></h4>
						</div>
						<div class="bottom">
							<?php 
							switch ($worth) {
								case 1:
									$text = "A challenge worth 1 Life Point requires you add to a new behavior into your life. These challenges require your time and consciouseffort for success. Successfully completing these challenges is a step forwardon your journey to change.";
								break;
								case 3:
									$text = "A challenge worth 3 Life Points requires you to complete a task. These challenges require your time, effort, and energy forsuccess. Successfully completingthese challenges is a jump forward on your journey to change.";
								break;
								case 5:
									$text = "A challenge worth 5 Life Points required you to commit to a lifestyle change.  These challenges require your time, energy, and commitment tolong-term change. Successfullycompleting these challenges is a giant leap forward on your journey to change.";
								break;
							}
							?>
							<p><?php echo $text; ?></p>
							<a href="<?php echo get_permalink(1011); ?>">CHECK OUT THE CHALLENGE BOARD</a>
						</div>
					</div>
					<?php echo $worth; ?>
				</div>
				<?php the_post_thumbnail('post-featured'); ?>
				<script>
					$(function(){
						$(".attachment-post-featured").load(function() {
							$(this).css('margin-top', '-50px');
							var height = parseInt($(this).height()) - 60;
							var width = parseInt($(this).width()) - 60;
							var color = $("#crumbs").css('background');
							$(".post-points").css({
								'top': height,
								'left': width,
								'background': color
							});
							$(".post-points .hover .top").css('background', color);
						});
					});
					$(".post-points").hover(function() {
						$(this).find(".hover").fadeIn();
					}, function() {
						$(this).find(".hover").fadeOut();
					});
				</script>
			</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>

		<div class="entry-meta">
			<?php cboard_link() ?> 
			<?php twentythirteen_entry_meta(); ?>
			<?php $authorextra = get_post_meta($post->ID, 'wpcf-additional-author-info', TRUE);
				if (!$authorextra) {
					echo '';
					}
					else { ?>
					<span class="authorextra"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/heart.png"><?php echo $authorextra; ?></span>
			<?php } ?>
	
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	
	<?php echo do_shortcode('[social_buttons]'); ?>

	<?php if ( is_search() || is_category()) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	
	<div class="extra-drop-downs">

		<?php $variations = get_post_meta($post->ID, 'variations', TRUE); ?>
		<?php $tips = get_post_meta($post->ID, 'beginners_tips', TRUE); ?>
		<?php $wisdom = get_post_meta($post->ID, 'yoffie_wisdom', TRUE); ?>
		<?php $benefits = get_post_meta($post->ID, 'health_benefits', TRUE); ?>

		<?php if (!$variations){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Variations" id="variations" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Variations"]' . $variations . '[/expand]'); 
			} ?>
		<?php if (!$tips){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Beginner\'s Tips" id="tips" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Beginner\'s Tips"]' . $tips . '[/expand]'); 
			} ?>
		<?php if (!$benefits){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Health Benefits" id="benefits" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Health Benefits"]' . $benefits . '[/expand]'); 
			} ?>
		<?php if (!$wisdom){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Yoffie Wisdom" id="wisdom" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Yoffie Wisdom"]' . $wisdom . '[/expand]'); 
			} ?>
		<?php $facts = get_post_meta($post->ID, 'the_facts', TRUE); ?>
		<?php $symptoms = get_post_meta($post->ID, 'the_symptoms', TRUE); ?>
		<?php $what = get_post_meta($post->ID, 'what_is_happening', TRUE); ?>
		<?php $why = get_post_meta($post->ID, 'why_is_this_happening', TRUE); ?>
		<?php $lifestyle = get_post_meta($post->ID, 'lifestyle_adjustments', TRUE); ?>
		<?php $prevent = get_post_meta($post->ID, 'prevent_it', TRUE); ?>
		<?php $fix = get_post_meta($post->ID, 'fix_it', TRUE); ?>
		<?php $exercises = get_post_meta($post->ID, 'exercises', TRUE); ?>

		<?php if (!$facts){
			echo '';
			} else {
			echo do_shortcode('[expand title="<span class="print-no"><img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> The Facts</span>" id="the-facts" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> The Facts"]' . $facts . '[/expand]'); 
			} ?>
		<?php if (!$symptoms){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> The Symptoms" id="symptoms" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> The Symptoms"]' . $symptoms . '[/expand]'); 
			} ?>
		<?php if (!$what){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> What Is Happening?" id="what" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> What Is Happening?"]' . $what . '[/expand]'); 
			} ?>
		<?php if (!$why){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Why Is This Happening" id="why" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Why Is This Happening"]' . $why . '[/expand]'); 
			} ?>
		<?php if (!$lifestyle){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Lifestyle Adjustments" id="lifestyle" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Lifestyle Adjustments"]' . $lifestyle . '[/expand]'); 
			} ?>
		<?php if (!$prevent){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Prevent It" id="prevent" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Prevent It"]' . $prevent . '[/expand]'); 
			} ?>
		<?php if (!$fix){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Fix It" id="fix" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Fix It"]' . $fix . '[/expand]'); 
			} ?>
		<?php if (!$exercises){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Exercises" id="exercises" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Exercises"]' . $exercises . '[/expand]'); 
			} ?>

	</div><!-- extra drop downs -->

		<?php
		// Find connected pages
			$connected = new WP_Query( array(
  			'connected_type' => 'exercises_to_posts',
  			'connected_items' => get_queried_object(),
  			'nopaging' => true,
		) );

		// Display connected pages
		if ( $connected->have_posts() ) :
		?>
		<h3>THE SEQUENCE:</h3>
		<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		<a class="workout-exercise-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<div class="exercise-photos">

		<?php $firstphoto = get_post_meta($post->ID, 'wpcf-image-1', raw); ?>
		<?php $secondphoto = get_post_meta($post->ID, 'wpcf-image-2', raw); ?>
		<?php $thirdphoto = get_post_meta($post->ID, 'wpcf-image-3', raw); ?>
		<?php $fourthphoto = get_post_meta($post->ID, 'wpcf-image-4', raw); ?>
		<?php $firstcaption = get_post_meta($post->ID, 'wpcf-image-1-caption', TRUE); ?>
		<?php $secondcaption = get_post_meta($post->ID, 'wpcf-image-2-caption', TRUE); ?>
		<?php $thirdcaption = get_post_meta($post->ID, 'wpcf-image-3-caption', TRUE); ?>
		<?php $fourthcaption = get_post_meta($post->ID, 'wpcf-image-4-caption', TRUE); ?>

		<?php if (!$fourthphoto) {
				if (!$thirdphoto) {
					if (!$secondphoto) {
					echo '<div class="one-photo"><div class="exercise-grid"><img src="'; echo $firstphoto; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div></div>';
					} else {
					echo '<div class="two-photos"><div class="exercise-grid"><img src="'; echo $firstphoto; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $secondphoto; echo'"><div class="exercise-caption">'; echo $secondcaption; echo '</div></div></div>';
					}
				} else {
				echo '<div class="three-photos"><div class="exercise-grid"><img src="'; echo $firstphoto; echo'"><div class="exercise-caption">'; echo $firstcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $secondphoto; echo'"><div class="exercise-caption">'; echo $secondcaption; echo '</div></div><div class="exercise-grid"><img src="'; echo $thirdphoto; echo'"><div class="exercise-caption">'; echo $thirdcaption; echo '</div></div></div>';
				}
		      	} else {
			echo '<div class="four-photos"><div class="exercise-grid"><img src="'; echo $firstphoto; echo'"></div><div class="exercise-grid"><img src="'; echo $secondphoto; echo'"></div><div class="exercise-grid"><img src="'; echo $thirdphoto; echo'"></div><div class="exercise-grid"><img src="'; echo $fourthphoto; echo'"></div>';
		} ?>
		
		</div><!-- end exercise-photos -->
			<a class="learn-more-exercise" href="<?php the_permalink(); ?>">Learn more about this exercise &#8594;</a>
			<div style="clear: both;"></div>
		<?php endwhile; ?>

		<?php 
		// Prevent weirdness
		wp_reset_postdata();

		endif;
		?>
		
	</div><!-- .entry-content -->
	<div class="second-social"><?php echo do_shortcode('[social_buttons]'); ?></div>
	<div class="disclaimer">This information is not intended to replace the advice of a doctor. Yoffie Life disclaims any liability for the decisions you make based on this information.</div>
	<?php endif; ?>
	
	<footer class="entry-meta">
		<?php twentythirteen_cats_tags(); ?>
		



		
		
	</footer><!-- .entry-meta -->

</div>	
</article><!-- #post -->