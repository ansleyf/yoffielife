<?php
/**
Template Name: Services
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>

						<div id="services-categories">
							<a data-modal="nutrition" href="#" >
								<div class="sub-category-button">
									<span>Nutrition</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/nutrition.png">
								</div>
							</a>
							<a data-modal="cleanse" href="#" >
								<div class="sub-category-button">
									<span>Real Food Cleanse</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/real-food-cleanse.png">
								</div>
							</a>
							<a data-modal="cooking" href="#" >
								<div class="sub-category-button">
									<span>Cooking</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/cooking.png">
								</div>
							</a>
							<a data-modal="therapy" href="#" >
								<div class="sub-category-button">
									<span>Body Therapy & Fitness</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/exercise-therapy.png">
								</div>
							</a>
							<a data-modal="organization" href="#">
								<div class="sub-category-button">
									<span>Organization</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/organization.png">
								</div>
							</a>
							<a data-modal="coaching" href="#" >
								<div class="sub-category-button">
									<span>Wellness Coaching</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/wellness-coaching.png">
								</div>
							</a>
							<a data-modal="beauty" href="#" >
								<div class="sub-category-button">
									<span>Beauty & Fashion</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/beauty-fashion.png">
								</div>
							</a>
							<a data-modal="corporate" href="#" >
								<div class="sub-category-button">
									<span>Corporate Services</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/corporate-services.png">
								</div>
							</a>
							<a data-modal="seminars" href="#" >
								<div class="sub-category-button">
									<span>Seminars & Speaking</span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/seminars-speaking.png">
								</div>
							</a>

						</div>
							<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation</a> on how we can customize a Yoffie Life seminar or speaking engagement for you!</p>

						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
				</article><!-- #post -->

			<?php endwhile; ?>
		</div><!-- #content -->
	</div><!-- #primary -->
	<div class="overlays services">
		<div class="overlay" id="nutrition">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Nutrition</h2>
			<p><span class="services-sub">Learn about Food. Understand Your Body. Find Balance.</span></p>
			<p>Are you tired of feeling exhausted? Do you need more energy? Do you have nagging sweet cravings that you can’t seem to control? Make a commitment to feeling good and looking great in your own body. Yoffie Life experts work with you to create a personalized nutrition plan and support you as you take a series of small steps to reach your health, weight, and wellness goals.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="cleanse">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Real Food Turnaround</h2>
			<p><span class="services-sub">Eat Real. Be Restored. Feel Vibrant.</span></p>
			<p>The Yoffie Life Real Food Turnaround supports a commitment to a healthier and simplified diet with a culinary experience in wholesome, nutrient-dense foods delivered to your door. </p>

			<p>Our philosophy is simple but strong: eating whole foods brings your body to its natural balance. Balance will help you gain freedom from cravings; put you on the path to achieving your ideal weight; and help you achieve a healthier, more energetic way of living. </p>

			<p>We only cook with real food that comes from the earth, never factories or science labs. There are no chemical additives, preservatives, or highly processed foods in our kitchen! We don’t believe in extremes, tricks, or gimmicks—just great-tasting, quality, natural whole foods that nourish wellness. Start your journey with a 1-day, 7-day, 14-day, or 28-day whole foods cleanse.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a> </p>
		</div>
		<div class="overlay" id="cooking">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Cooking</h2>
			<p><span class="services-sub">Empower Yourself. Improve Your Eating Habits.</span></p>
			<p>Yoffie Life Cooking offers cooking services to support a healthy and nutritional diet. We focus on seasonal, locally grown, organic, and unprocessed whole foods. Programs include educational green market tours and pantry makeovers, cooking classes (individual or group), and meal delivery and catering services. Choose a Yoffie Life Cooking program to begin taking small steps to achieve your wellness goals.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="therapy">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Body Therapy and Fitness</h2>
			<p><span class="services-sub">Strengthen Your Body. Increase Your Energy. Achieve Your Objectives.</span></p>
			<p>Begin taking small steps to achieve goals you never thought attainable. Yoffie Life Body Therapy and Fitness experts work with you to develop personalized fitness regimes, drawing upon ancient principles and techniques developed in the East and blending them with cutting-edge science from the West. In addition to instruction, Yoffie Life experts teach you about your body and provide you with the necessary feedback to help you hold yourself accountable to achieve your sports, health, and life goals.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="organization">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Organization</h2>
			<p><span class="services-sub">Organize Your Space. Organize Your Mind. Bring Balance to Your Life.</span></p>
			<p>Are you overwhelmed? Do you feel consumed by the general clutter in your home or office? Yoffie Life offers services that tackle the space, objects, and chores that bog you down and keep you from doing (and feeling) your best. We work with you to customize a program to bring your home, office, and life back into balance.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="coaching">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Wellness Coaching</h2>
			<p><span class="services-sub">Set Goals. Modify Behaviors. Attain Optimal Wellness.</p>
			<p>Yoffie Life wellness coaches work with you to develop a plan to help you achieve sustainable lifelong wellness in concert with your health and lifestyle goals. We offer personalized support and motivational tools to help you focus on making practical decisions. Emphasis is placed on goal setting and how to turn goals into action.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="beauty">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Beauty and Yoffie Life Fashion</h2>
			<p><span class="services-sub">Learn about Food. Understand Your Body. Find Balance.</span></p>
			<p>Highlight the best parts of you! The right makeup, skin care routine, and clothes make you feel like your most beautiful self. Yoffie Life Fashion offers personalized shopping and closet editing services. Yoffie Life Beauty offers makeup and skin care consultations; at-home makeup lessons; and spa, beauty, and antiaging facials. Each service is designed to cater to your beauty and fashion goals.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
		</div>
		<div class="overlay" id="corporate">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Corporate Services</h2>
			<p><span class="services-sub">Studies show that medical costs fall about $3.27 and the cost of absentee days falls about $2.37 for every dollar spent on wellness programs.* Yoffie Life offers on-site corporate wellness services to promote a healthy workforce. Take advantage of our nutrition, fitness, and cooking services in the form of lectures, demonstrations, and individual and group coaching programs. Our programs are designed to specifically meet the individual needs of your employees and workplace.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation.</a></p>
			<p style="font-size: .85em;">See the <a href="http://dash.harvard.edu/handle/1/5345879" target="_blank">Harvard Workplace Wellness</a> study</a>
		</div>
		<div class="overlay" id="seminars">
			<a href="#" class="overlay-close">X</a>
			<h2>Yoffie Life Speaking and Seminars</h2>
			<p><span class="services-sub">Share the Message of Incremental Wellness.</span></p>
			<p>Yoffie Life speakers and seminar leaders guide discussions on nutrition, fitness, organization, and balanced wellness. Our message is simple: taking small, educated steps toward wellness yields big benefits for a healthier life over time. Share the Yoffie Life Experience with your company, organization, friends, and family.</p>
			<p><a href="mailto:yoffielife@gmail.com">Email us today at yoffielife@gmail.com to schedule a free consultation</a> on how we can customize a Yoffie Life seminar or speaking engagement for you!</p>
		</div>
	</div>
<?php get_footer(); ?>