<?php
/*
Template Name: Satisfy Dictionary Entry
*/
?>

<?php get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<?php if ( is_search() || is_category()) : ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail('excerpt-thumb'); ?>
		</div>
		<?php else : ?>
		<div class="entry-thumbnail">
			<?php get_template_part( 'breadcrumbs'); ?>
			<?php the_post_thumbnail('post-featured'); ?>
		</div>
		<?php endif; ?>
		<?php endif; ?>

		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>

		<div class="entry-meta">
			<?php cboard_link() ?>
			<?php twentythirteen_entry_meta(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	<?php echo do_shortcode('[social_buttons]'); ?>

	<?php if ( is_search() || is_category()) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	<div class="extra-drop-downs">
		<?php $identification = get_post_meta($post->ID, 'the_identification', TRUE); ?>
		<?php $taste = get_post_meta($post->ID, 'taste', TRUE); ?>
		<?php $availability = get_post_meta($post->ID, 'availability', TRUE); ?>
		<?php $season = get_post_meta($post->ID, 'season', TRUE); ?>
		<?php $selection = get_post_meta($post->ID, 'selection', TRUE); ?>
		<?php $organic = get_post_meta($post->ID, 'organic_benefits', TRUE); ?>
		<?php $storage = get_post_meta($post->ID, 'storage', TRUE); ?>
		<?php $preparation = get_post_meta($post->ID, 'preparation', TRUE); ?>
		<?php $cutting = get_post_meta($post->ID, 'cutting_serving', TRUE); ?>
		<?php $wine = get_post_meta($post->ID, 'wine_pairing', TRUE); ?>
		<?php $digestibility = get_post_meta($post->ID, 'digestibility', TRUE); ?>
		<?php $substitutions = get_post_meta($post->ID, 'substitutions', TRUE); ?>
		<?php $food = get_post_meta($post->ID, 'food_pairing', TRUE); ?>
		<?php $nutrition = get_post_meta($post->ID, 'nutrition_summary', TRUE); ?>
		<?php $benefits = get_post_meta($post->ID, 'health_benefits_medical_claims', TRUE); ?>
		<?php $facts = get_post_meta($post->ID, 'the_little_known_facts', TRUE); ?>

		<?php if (!$identification){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Identification" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Identification"]' . $identification . '[/expand]'); 
			} ?>
		<?php if (!$taste){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Taste" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Taste"]' . $taste . '[/expand]'); 
			} ?>
		<?php if (!$availability){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Availability" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Availability"]' . $availability . '[/expand]'); 
			} ?>
		<?php if (!$season){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Season" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Season"]' . $season . '[/expand]'); 
			} ?>
		<?php if (!$selection){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Selection" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Selection"]' . $selection . '[/expand]'); 
			} ?>
		<?php if (!$organic){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Organic Benefits" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Organic Benefits"]' . $organic . '[/expand]'); 
			} ?>
		<?php if (!$storage){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Storage" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Storage"]' . $storage . '[/expand]'); 
			} ?>
		<?php if (!$preparation){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Preparation" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Preparation"]' . $preparation . '[/expand]'); 
			} ?>
		<?php if (!$cutting){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Cutting & Serving" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Cutting & Serving"]' . $cutting . '[/expand]'); 
			} ?>
		<?php if (!$wine){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Wine Pairings" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Wine Pairings"]' . $wine . '[/expand]'); 
			} ?>
		<?php if (!$digestibility){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Digestibility" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Digestibility"]' . $digestibility . '[/expand]'); 
			} ?>
		<?php if (!$substitutions){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Substitutions" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Substitutions"]' . $substitutions . '[/expand]'); 
			} ?>
		<?php if (!$food){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Food Pairings" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Food Pairings"]' . $food . '[/expand]'); 
			} ?>
		<?php if (!$nutrition){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Nutrition Summary" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Nutrition Summary"]' . $nutrition . '[/expand]'); 
			} ?>
		<?php if (!$benefits){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Health Benefits & Medical Claims" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Health Benefits & Medical Claims"]' . $benefits . '[/expand]'); 
			} ?>
		<?php if (!$facts){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Little Known Facts" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Little Known Facts"]' . $facts . '[/expand]'); 
			} ?>
		

	</div><!-- end extra drop downs -->
		
	</div><!-- .entry-content -->
	<div class="second-social"><?php echo do_shortcode('[social_buttons]'); ?></div>
	<div class="disclaimer">This information is not intended to replace the advice of a doctor. Yoffie Life disclaims any liability for the decisions you make based on this information.</div>
	<?php endif; ?>

	<footer class="entry-meta">
		<?php twentythirteen_cats_tags(); ?>
		<?php echo do_shortcode( '[manual_related_posts]' ); ?>
		<?php twentythirteen_post_nav(); ?>
		<?php comments_template(); ?> 

		
		
	</footer><!-- .entry-meta -->

	
</article><!-- #post -->
				
				

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>