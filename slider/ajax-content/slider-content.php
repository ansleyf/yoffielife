 <div id="tabbed-nav">
                    <ul>
                        <li class="spark" ><a>Spark<span>Love, Family and Friends</span></a></li>
                        <li class="simmer" ><a>Simmer<span>Healthful Recipes & Cooking</span></a></li>
                        <li class="soul" ><a>Soul<span>Inspiration & Insight</span></a></li>
                        <li class="sweat" ><a>Sweat<span>Fitness & Integrative Therapy</span></a></li>                        
                        <li class="simplify" ><a>Simplify<span>Organization & Productivity</span></a></li>
			<li class="satisfy" ><a>Satisfy<span>Holistic Nutrition</span></a></li>
                    </ul>

                    <div>
                        <div>
                        <?php query_posts('category_name=spark&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>

                        <div>
                        <?php query_posts('category_name=simmer&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>

                        <div>
                        <?php query_posts('category_name=soul&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>

                        <div>
                        <?php query_posts('category_name=sweat&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>


			<div>
                        <?php query_posts('category_name=simplify&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>

			<div>
                        <?php query_posts('category_name=satisfy&cat=210&showposts=1'); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<?php the_post_thumbnail('category-feature'); ?>
				</div>
				<h1 class="entry-title"><?php the_title(); ?><br><span>View Challenge &#8594;</span></h1></a>
			<?php endwhile; ?>   
            		<?php wp_reset_query(); ?>
			</div>


                    </div>
                    </div>