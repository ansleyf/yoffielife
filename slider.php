<div id="basic-usage">
	<div id="tabbed-nav">
		<ul>
			<li class="satisfy" >
				<div class="arrow-left"></div>
				<a data-href="./satisfy/">Satisfy<span>Nutrition Facts & Motivation</span></a>
			</li>
			<li class="simmer" >
				<div class="arrow-left"></div>
				<a data-href="./simmer/">Simmer<span>Healthful Recipes & Cooking Tips</span></a>
			</li>
			<li class="simplify" >
				<div class="arrow-left"></div>
				<a data-href="./simplify/">Simplify<span>Organization & Beauty</span></a>
			</li>
			<li class="soul" >
				<div class="arrow-left"></div>
				<a data-href="./soul/">Soul<span>Mindfulness and Self-Care</span></a>
			</li>
			<li class="spark" >
				<div class="arrow-left"></div>
				<a data-href="./spark/">Spark<span>Love, Family & Friends</span></a>
			</li>
			<li class="sweat" >
				<div class="arrow-left"></div>
				<a data-href="./sweat/">Sweat<span>Fitness & Integrative Therapy</span></a>
			</li>
			<li class="scoop" >
				<div class="arrow-left"></div>
				<a data-href="./weekly-scoop/">Scoop<span>Victoria's Tips & Insight</span></a>
			</li>
		</ul>
		<div>
			<div>
				<?php
					global $do_not_duplicate;
					$satisfy = array(
						"post_type" => array("post", "satisfy_dictionary"),
						"category_name" => "satisfy",
						"showposts" => 1,
						"cat" => 210
					);
					query_posts($satisfy);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>
					<a href="<?php the_permalink(); ?>">
						<div class="entry-thumbnail">
							<div class="title">
								This Week's Yoffie Life Challenges
							</div>
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
						</div>
						<div class="entry-container">
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<div class="view satisfy">
								
								<div class="right">View Satisfy Challenge</div>
							</div>
						</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
				<?php 
					$simmer_dictionary = array(
						"post_type" => array("post", "simmer_dictionary"),
						"category_name" => "simmer",
						"cat" => 210,
						"showposts" => 1
					);
					query_posts($simmer_dictionary);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
				?>
					<a href="<?php the_permalink(); ?>">
					<div class="entry-thumbnail">
						<div class="title">
							This Week's Yoffie Life Challenges
						</div>
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
					</div>
					<div class="entry-container">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="view simmer">
							
							<div class="right">View Simmer Challenge</div>
						</div>
					</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
				<?php
					$simplify = array(
						"category_name" => "simplify",
						"cat" => 210,
						"showposts" => 1
					);
					query_posts($simplify);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
				?>
					<a href="<?php the_permalink(); ?>">
					<div class="entry-thumbnail">
						<div class="title">
							This Week's Yoffie Life Challenges
						</div>
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
					</div>
					<div class="entry-container">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="view simplify">
							
							<div class="right">View Simplify Challenge</div>
						</div>
					</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
				<?php 
					$soul = array(
						"category_name" => "soul",
						"cat" => 210,
						"showposts" => 1
					);
					query_posts($soul);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
				?>
					<a href="<?php the_permalink(); ?>">
					<div class="entry-thumbnail">
						<div class="title">
							This Week's Yoffie Life Challenges
						</div>
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
					</div>
					<div class="entry-container">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="view soul">
							
							<div class="right">View Soul Challenge</div>
						</div>
					</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
				<?php
					$spark = array(
						"category_name" => "spark",
						"cat" => 210,
						"showposts" => 1
					);
					query_posts($spark);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
				?>
					<a href="<?php the_permalink(); ?>">
					<div class="entry-thumbnail">
						<div class="title">
							This Week's Yoffie Life Challenges
						</div>
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
					</div>
					<div class="entry-container">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="view spark">
							
							<div class="right">View Spark Challenge</div>
						</div>
					</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
				<?php 
					$exercises =array(
						"post_type" => array("post", "exercises", "sweat_dictionary"),
						"category_name" => "sweat",
						"cat" => 210,
						"showposts" => 1
					);
					query_posts($exercises);
					while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
				?>
					<a href="<?php the_permalink(); ?>">
					<div class="entry-thumbnail">
						<div class="title">
							This Week's Yoffie Life Challenges
						</div>
						<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
					</div>
					<div class="entry-container">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="view sweat">
							
							<div class="right">View Sweat Challenge</div>
						</div>
					</div>
					</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<div>
			<?php
				$yoffiescoop = array(
					"category_name" => "yoffie-scoop",
					"cat" => 210,
					"showposts" => 1
				);
				query_posts($yoffiescoop);
				while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID;
			?>
				<a href="<?php the_permalink(); ?>">
				<div class="entry-thumbnail">
					<div class="title">
							This Week's Yoffie Life Challenges
						</div>
					<?php if ( has_post_thumbnail() ) { the_post_thumbnail('category-feature'); } else { echo '<img src="/wp-content/themes/twentythirteen-child/images/category-placeholder.jpg">'; } ?>
				</div>
				<div class="entry-container">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<div class="view scoop">
						
						<div class="right">View Scoop Challenge</div>
					</div>
				</div>
				</a>
				<?php endwhile; wp_reset_query(); ?>
			</div>

        </div>
    </div>
</div>
<div style="clear: both;"></div>