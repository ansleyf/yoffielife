<?php
/*
	Template Name: Challenge Board
*/

get_header(); ?>
<?php get_sidebar('cboard'); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="board badges">
			<?php the_badges(); ?>
		</div>
		<header class="entry-header">
			<h1 class="entry-title">Challenge Board</h1>
		</header>
		<?php if($cboard->mgoalt == 0 && is_user_logged_in()) : ?>
		<div class="board setgoalbanner">
			<div class="left">
				<h2>Set Your Goal</h2>
				<h4>
					How many challenges can you complete in a month?
					Set your goal and get rewarded for your efforts!
				</h4>
			</div>
			<div class="right">
				<form id="updategoalformb">
					<label for="goal">
						<input type="number" name="goal" min="1">
						Challenges Per Month
					</label>
					<input type="submit" value="Commit">
				</form>
			</div>
		</div>
		<?php endif; ?>
			<?php
				$incomp = $cboard->get_incomplete();
				$comple = $cboard->get_completed();
			?>
			<ul class="triple-grid chal">
			<?php
				while($incomp->fetch()) :
				$post = $incomp->raw("chal");
				$post = get_post($post);
			?>
				<li class="id-<?php echo get_the_ID(); ?>"><?php if ( has_post_thumbnail() ) : ?>
	                <div class="triple-thumbnail">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('triple-grid'); ?></a>
	                </div>
	                <?php else : ?>
	                <div class="category-thumbnail">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><img src="/wp-content/themes/twentythirteen-child/images/white-y.png"></a>
	                </div>
	                <?php endif; ?>
	                <div class="triple-grid-overlay">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
	                <?php cboard_link("complete", true); ?>
	                </div>
        		</li>
			<?php endwhile; ?>
			</ul>
			<header class="entry-header">
				<h1 class="entry-title">Victories</h1>
			</header>
			<ul class="triple-grid vic">
			<?php
				while($comple->fetch()) :
				$post = $comple->raw("chal");
				$post = get_post($post);
			?>
				<li class="id-<?php echo get_the_ID(); ?>"><?php if ( has_post_thumbnail() ) : ?>
	                <div class="triple-thumbnail">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('triple-grid'); ?></a>
	                </div>
	                <?php else : ?>
	                <div class="category-thumbnail">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><img src="/wp-content/themes/twentythirteen-child/images/white-y.png"></a>
	                </div>
	                <?php endif; ?>
	                <div class="triple-grid-overlay">
	                	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo ShortenText(get_the_title()); ?></a>
	                <?php cboard_link("remove", true); ?>
	                </div>
        		</li>
			<?php endwhile; ?>
			</ul>
			<?php while ( have_posts() ) : the_post(); global $currpage; $currpage = get_the_ID(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
				</article><!-- #post -->
			<?php endwhile; wp_reset_query(); ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>