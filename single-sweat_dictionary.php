<?php
/*
Template Name: Sweat Dictionary Entry
*/
?>

<?php get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<?php if ( is_search() || is_category()) : ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail('excerpt-thumb'); ?>
		</div>
		<?php else : ?>
		<div class="entry-thumbnail">
			<?php get_template_part( 'breadcrumbs'); ?>
			<?php the_post_thumbnail('post-featured'); ?>
		</div>
		<?php endif; ?>
		<?php endif; ?>

		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>

		<div class="entry-meta">
			<?php cboard_link() ?>
			<?php twentythirteen_entry_meta(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	<?php echo do_shortcode('[social_buttons]'); ?>

	<?php if ( is_search() || is_category()) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

	<div class="extra-drop-downs">
		<?php $facts = get_post_meta($post->ID, 'the_facts', TRUE); ?>
		<?php $symptoms = get_post_meta($post->ID, 'the_symptoms', TRUE); ?>
		<?php $what = get_post_meta($post->ID, 'what_happening', TRUE); ?>
		<?php $why = get_post_meta($post->ID, 'why_happening', TRUE); ?>
		<?php $lifestyle = get_post_meta($post->ID, 'lifestyle_adjustments', TRUE); ?>
		<?php $prevent = get_post_meta($post->ID, 'prevent_it', TRUE); ?>
		<?php $fix = get_post_meta($post->ID, 'fix_it', TRUE); ?>
		<?php $exercises = get_post_meta($post->ID, 'rehab_exercises', TRUE); ?>

		<?php if (!$facts){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> The Facts" id="the-facts" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> The Facts"]' . $facts . '[/expand]'); 
			} ?>
		<?php if (!$symptoms){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> The Symptoms" id="symptoms" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> The Symptoms"]' . $symptoms . '[/expand]'); 
			} ?>
		<?php if (!$what){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> What Is Happening" id="what" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> What Is Happening"]' . $what . '[/expand]'); 
			} ?>
		<?php if (!$why){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Why Is This Happening" id="why" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Why Is This Happening"]' . $why . '[/expand]'); 
			} ?>
		<?php if (!$lifestyle){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Lifestyle Adjustments" id="lifestyle" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Lifestyle Adjustments"]' . $lifestyle . '[/expand]'); 
			} ?>
		<?php if (!$prevent){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Prevent It" id="prevent" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Prevent It"]' . $prevent . '[/expand]'); 
			} ?>
		<?php if (!$fix){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Fix It" id="fix" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Fix It"]' . $fix . '[/expand]'); 
			} ?>
		<?php if (!$exercises){
			echo '';
			} else {
			echo do_shortcode('[expand title="<img src=\'/wp-content/themes/twentythirteen-child/images/down-arrow.png\'> Exercises" id="exercises" swaptitle="<img src=\'/wp-content/themes/twentythirteen-child/images/up-arrow.png\'> Exercises"]' . $exercises . '[/expand]'); 
			} ?>

	</div><!-- end extra drop downs -->
		
	</div><!-- .entry-content -->
	<div class="second-social"><?php echo do_shortcode('[social_buttons]'); ?></div>
	<div class="disclaimer">This information is not intended to replace the advice of a doctor. Yoffie Life disclaims any liability for the decisions you make based on this information.</div>
	<?php endif; ?>

	<footer class="entry-meta">
		<?php twentythirteen_cats_tags(); ?>
		<?php echo do_shortcode( '[manual_related_posts]' ); ?>
		<?php twentythirteen_post_nav(); ?>
		<?php comments_template(); ?> 


		
		
	</footer><!-- .entry-meta -->

	
</article><!-- #post -->
				
				

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>