<?php if ( in_category( 'spark' )) { ?>

	<div id="crumbs" class="print-no spark"><a href="/spark"><div class="print-no back-arrow"></div>Spark</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 164 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'yoffie-scoop' )) { ?>

	<div id="crumbs" class="print-no scoop"><a href="/weekly-scoop"><div class="print-no back-arrow"></div>Scoop</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 385 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'simmer' )) { ?>

	<div id="crumbs" class="print-no simmer"><a href="/simmer"><div class="print-no back-arrow"></div>Simmer</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 162 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'soul' )) { ?>

	<div id="crumbs" class="print-no soul"><a href="/soul"><div class="print-no back-arrow"></div>Soul</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 160 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'sweat' )) { ?>

	<div id="crumbs" class="print-no sweat"><a href="/sweat"><div class="print-no back-arrow"></div>Sweat</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 163 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'simplify' )) { ?>

	<div id="crumbs" class="print-no simplify"><a href="/simplify"><div class="print-no back-arrow"></div>Simplify</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 165 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } elseif ( in_category( 'satisfy' )) { ?>

	<div id="crumbs" class="print-no satisfy"><a href="/satisfy"><div class="print-no back-arrow"></div>Satisfy</a>

     	<?php 
	$terms = get_the_terms( $post->ID, 'category' );
	$term = reset($terms);
	if ( $term->term_id == 210 || $term->term_id == 161 ) { }  else {
	echo '<div class="print-no back-arrow"></div><a href="'.get_term_link($term->slug, 'category').'">'.$term->name.'</a>';
	}
	?>
	</div>

<?php } else { }

?>