<?php
/*
Template Name: Front Page
*/

get_header(); global $do_not_duplicate; ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div id="loading">
			<img class='loader-image' src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader.gif"/>
			</div>
		    <div class="visible-large"><?php get_template_part( 'slider' ); ?></div>
		    <div class="visible-small">
			<ul class="mobile-tabbednav">
				<li class="satisfy" >
					<div class="arrow-left"></div>
					<a href="./satisfy/">Satisfy<span>Nutrition Facts & Motivation</span></a>
				</li>
				<li class="simmer" >
					<div class="arrow-left"></div>
					<a href="./simmer/">Simmer<span>Healthful Recipes & Cooking Tips</span></a>
				</li>
				<li class="simplify" >
					<div class="arrow-left"></div>
					<a href="./simplify/">Simplify<span>Organization & Beauty</span></a>
				</li>
				<li class="soul" >
					<div class="arrow-left"></div>
					<a href="./soul/">Soul<span>Mindfulness and Self-Care</span></a>
				</li>
				<li class="spark" >
					<div class="arrow-left"></div>
					<a href="./spark/">Spark<span>Love, Family & Friends</span></a>
				</li>
				<li class="sweat" >
					<div class="arrow-left"></div>
					<a href="./sweat/">Sweat<span>Fitness & Integrative Therapy</span></a>
				</li>
				<li class="scoop" >
					<div class="arrow-left"></div>
					<a href="./weekly-scoop/">Scoop<span>Victoria's Tips & Insight</span></a>
				</li>
				<li class="yoffie-blog" >
					<div class="arrow-left"></div>
					<a href="./letters-to-the-reader/">Victoria's Blog<span>Letters to the Reader</span></a>
				</li>
				<li class="challenge-board" >
					<div class="arrow-left"></div>
					<a href="./challenge-board/">Challenge Board<span>Challenge Yourself</span></a>
				</li>
			</ul>
		    </div>
		    <div class="hidden-large"><?php get_template_part( 'triple-grid-thisweek' ); ?></div>
			
		    <div id="welcome-yoffie">
			<?php get_sidebar(); ?>
			<div id="welcome-interior">
			<header  class="homepage-header">
				<h1>The Yoffie Life Challenge</h1>

			</header>
			<?php the_content(); ?>
			<!--<div id="home-signup">
				<div class="sign-up-home">
				<a class="join-button" href="<?php if ( !is_user_logged_in() ) { echo "/join"; } else { echo "/already-member"; } ?>" >Join!</a>
				<strong>Join Yoffie Life</strong> to gain access to our community forums and start your own challenge board!
				<span><em>Want to find out more about our Weekly Challenges First? <a href="/this-weeks-challenges">Learn more about the Yoffie Philosophy here</a>.</em></span>
				</div>
				<div class="sign-in-home">
				<strong>Already a member? Sign in here!</strong>
				<?php wp_login_form( $args ); ?>
				</div>

			</div>-->

			<?php
				$more = array(
					"post__not_in"		=> $do_not_duplicate,
					"posts_per_page"	=> 12,
					"cat"      		=> "-1172"
				);
				query_posts($more);
			?>
			<?php if ( have_posts() ) : ?>
			<header class="homepage-header" style="margin-top: 60px;">
				<h1>Challenge Yourself Today!</h1>
			</header>

			<?php /* The loop */ ?>
			<?php get_template_part( 'triple-grid' ); ?>
			
			<?php wp_reset_query(); ?>
			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

			</div>
		    </div>

		   <div style="clear: both;"></div>

			
		</div><!-- #content -->
	</div><!-- #primary -->

<script>
$(function(){
    /* jQuery activation and setting options for the tabs*/
    $("#tabbed-nav").zozoTabs({
    	responsive: false,
        theme: "silver",
        orientation: "vertical",
        position: "top-left",
        size: "large",
        defaultTab: "tab1",
		event: "mouseover",
        rounded: false,
        multiline: true,
        shadows:true,
        animation: {
            easing: "easeInOutExpo",
            duration: 300,
            effects: "slideV"
        },
		autoplay: {
			interval: 4000,
			smart: true
		}
    });
    $(".z-link").click(function(){
    	var link = $(this).attr("data-href");
    	window.location.href = link;
    });
});
</script>
<?php get_footer(); ?>